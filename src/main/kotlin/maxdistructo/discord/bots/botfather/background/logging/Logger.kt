package maxdistructo.discord.bots.botfather.background.logging

import maxdistructo.discord.bots.botfather.BaseBot
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter

class Logger : ListenerAdapter() {
    override fun onGuildMessageReceived(event: GuildMessageReceivedEvent){
        BaseBot.logger.info(" [" + event.guild.name + "] " + "#" + event.message.channel.name + " " + event.message.author.name + "#" + event.message.author.discriminator + ": " + event.message.contentDisplay)
    }

    override fun onPrivateMessageReceived(event: PrivateMessageReceivedEvent) {
        BaseBot.logger.info(" [PM]" + " " + event.message.author.name + "#" + event.message.author.discriminator + ": " + event.message.contentDisplay)
    }
}