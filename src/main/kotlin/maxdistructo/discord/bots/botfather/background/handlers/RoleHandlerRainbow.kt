package maxdistructo.discord.bots.botfather.background.handlers

import maxdistructo.discord.bots.botfather.BaseBot
import java.awt.Color

class RoleHandlerRainbow : RoleHandler{
    override val roleName = "Rainbow"
    override val applicableGuilds = mutableListOf(228112197237080066)
    var i = 0;
    override fun update() {
        val colors = listOf(Color.RED, Color(0xFFA500), Color(0xFFD500), Color.GREEN, Color.BLUE, Color(0x800080))
        for(guild in applicableGuilds) {
            val guild = BaseBot.client.getGuildById(guild)!!
            val role = guild.getRolesByName(roleName, true)[0]
            role.manager.setColor(colors[i]).submit()
            if(i >= 5) {
                i = 0
            }
            else{
                i++
            }
        }
    }
}