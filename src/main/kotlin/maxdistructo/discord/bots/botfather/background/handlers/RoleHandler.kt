package maxdistructo.discord.bots.botfather.background.handlers

interface RoleHandler: Handler {
    val roleName: String
    val applicableGuilds: MutableList<Long>
}