package maxdistructo.discord.bots.botfather.background

import maxdistructo.discord.core.JSONUtils
import org.json.JSONArray

object PrivUtils {
    fun arrayToList(array : Array<String>) : List<String> {
        val output : MutableList<String> = mutableListOf()
        for(value in array){
            output += value
        }
        return output
    }
    fun listToArray(list : List<String>) : Array<String>{
        val output : ArrayList<String> = arrayListOf()
        for(value in list){
            output.add(value)
        }
        return output.toTypedArray()
    }
    fun jsonArrayToList(jsonArray: JSONArray) : List<String?> {
        return JSONUtils.toStringList(jsonArray)!!
    }

}