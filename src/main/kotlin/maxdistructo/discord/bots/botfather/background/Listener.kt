package maxdistructo.discord.bots.botfather.background

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import com.jagrosh.jdautilities.command.CommandListener
import maxdistructo.discord.bots.botfather.background.handlers.RoleHandlerRainbow
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.entities.ChannelType

class Listener : CommandListener {

    override fun onCompletedCommand(event: CommandEvent, command: Command?) { //When command is done, delete the message that triggered it
        if(event.channelType != ChannelType.PRIVATE) {
            event.message.delete()
        }
    }

    override fun onCommandException(event: CommandEvent, command: Command?, throwable: Throwable) {
        println(throwable.localizedMessage)
        Messages.sendDM(event.message.author, "You have entered an invalid command. Please check how it is run using !help.")
    }


}