package maxdistructo.discord.bots.botfather.background

import maxdistructo.discord.bots.botfather.BaseBot
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member

object Perms{
    var admins: MutableList<Long> = mutableListOf()
    var mods: MutableList<Long> = mutableListOf()
    init{
        admins.add(BaseBot.bot.commandAPI.ownerIdLong)
        mods.add(BaseBot.bot.commandAPI.ownerIdLong)
    }
    fun checkMod(member: Member) : Boolean{
        val permissions = member.permissions
        return when {
            mods.contains(member.idLong) -> {
                true
            }
            //In most instances, if you can kick people, you are a mod
            permissions.contains(Permission.KICK_MEMBERS) -> {
                true
            }
            checkAdmin(member) -> {
                true
            }
            else ->{
                false
            }
        }
    }
    fun checkAdmin(member: Member) : Boolean{
        val permissions = member.permissions
        return when {
            admins.contains(member.idLong) -> {
                true
            }
            //In most instances, if you can kick people, you are a mod
            permissions.contains(Permission.BAN_MEMBERS) -> {
                true
            }
            else ->{
                false
            }
        }
    }

}