package maxdistructo.discord.bots.botfather

import ch.qos.logback.classic.Level
import ch.qos.logback.classic.Logger
import com.jagrosh.jdautilities.examples.command.PingCommand
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import maxdistructo.discord.bots.botfather.background.Listener
import maxdistructo.discord.bots.botfather.background.handlers.Handler
import maxdistructo.discord.bots.botfather.background.handlers.RoleHandlerRainbow
import maxdistructo.discord.bots.botfather.commands.MafiaCommands
import maxdistructo.discord.bots.botfather.commands.mafia.data.RoleTrivaRetriever
//import maxdistructo.discord.bots.botfather.background.coreadditions.Bot
import maxdistructo.discord.bots.botfather.commands.standard.*
import maxdistructo.discord.core.jda.Config
import maxdistructo.discord.core.jda.impl.Bot
import net.dv8tion.jda.api.JDA
import java.util.*

object BaseBot{
    lateinit var bot : Bot
    lateinit var client: JDA
    lateinit var logger: Logger
    val ownerId: Long = 228111371965956099
    var activeTrivia : MutableList<Int> = mutableListOf()
    var nextTrivia = 0
    private val handlers = LinkedList<Handler>()

    @JvmStatic
    fun main(args : Array<String>){
        bot = Bot(Config.readToken())
        bot.setOwnerId(ownerId)
        bot.registerListeners(maxdistructo.discord.bots.botfather.background.logging.Logger())
        bot.registerCommands(
                PingCommand(),
                Check(),
                //Fortune(), TODO(Figure out why this broke. API End Issue)
                Horoscope(),
                Insult(),
                MafiaCommands.MafiaBaseCommand()
        )
        PlayerFun.pfCommands.forEach{pfCommand -> bot.registerCommand(pfCommand)}
        bot.init()
        bot.commandAPI.listener = Listener()
        client = bot.client
        logger = bot.logger
        logger.level = Level.INFO
        handlers.add(RoleHandlerRainbow())
        GlobalScope.launch{
            while(true) { //Launch each global loop into its own thread
                handlerLoop()
                Thread.sleep(5000)
            }
        }
    }

    fun handlerLoop(){
        logger.info("Running Handler Loop")
        for(handler in handlers){
            handler.update()
        }
    }

}

