package maxdistructo.discord.bots.botfather.commands.mafia

import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.TextChannel

object Perms {
    fun allowRead(channel: TextChannel, id: Long) {
        val member = channel.guild.getMemberById(id)!!
        removePerms(channel, id)
        channel.manager.putPermissionOverride(member, listOf(Permission.MESSAGE_READ), listOf())
    }

    fun allowReadWrite(channel: TextChannel, id: Long) {
        val member = channel.guild.getMemberById(id)!!
        removePerms(channel, id)
        channel.manager.putPermissionOverride(member, listOf(Permission.MESSAGE_READ, Permission.MESSAGE_WRITE),
                listOf())
    }

    fun removePerms(channel: TextChannel, id: Long) {
        val member = channel.guild.getMemberById(id)!!
        channel.getPermissionOverride(member)!!.delete().complete(true)
    }
}
