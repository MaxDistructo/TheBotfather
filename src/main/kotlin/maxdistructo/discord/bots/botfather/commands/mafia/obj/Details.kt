package maxdistructo.discord.bots.botfather.commands.mafia.obj

enum class Details(val cfgName : String) {
    ATTACK("attack"),
    DEFENCE("defence"),
    FRAMED("framed"),
    TIER("tier"),
    ALIVE("alive"),
    WINS("wins"),
    LOSSES("losses"),
    REVEALED("revealed"),
    DISGUISED("disguised"),
    JAILED("jailed"),
    RELAY("relay"),
    ROLE("role"),
    CLEANED("cleaned")
}