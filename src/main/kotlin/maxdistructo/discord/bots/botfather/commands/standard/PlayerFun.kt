package maxdistructo.discord.bots.botfather.commands.standard

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.jda.JDAUtils
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Member
import org.reflections.Reflections
import java.util.*

object PlayerFun //Additions Ideas: Shoot, Stab, Fake mute
{
    val pfCommands: LinkedList<Command> = LinkedList()

    init{
        val reflection = Reflections("maxdistructo.discord.bots.botfather.commands.standard")
        val classes = reflection.getSubTypesOf(BaseCommand::class.java)
        for(c in classes){
            pfCommands.add(PFCommand(c.newInstance()))
        }
    }

    //BaseCommand Wrapper for JDA Utilities
    private class PFCommand(val command: BaseCommand) : Command(){
        init{
            this.name = command.commandName
            this.help = command.helpMessage
        }
        override fun execute(event: CommandEvent) {
            event.reply(command.init(event.message, event.message.contentRaw.split(" ")))
        }
    }
    //Legacy BaseCommand private class
    private interface BaseCommand{
         val commandName: String;
         val helpMessage: String
         val hasOutput: Boolean
         fun init(message: Message, args: List<String>): String
    }

    private class Slap : BaseCommand{
        override val commandName: String
            get() = "slap"
        override val helpMessage: String
            get() = "slap <@User> - Slaps another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onSlapCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onSlapCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            mentioned.asMention + " you have been slapped by " + message.author.asMention + ":clap:"
        } else {
            message.jda.selfUser.name +" has slapped you " + message.author.asMention + " :clap:"
        }
    }

    private class Tnt : BaseCommand{
        override val commandName: String
            get() = "tnt"
        override val helpMessage: String
            get() = "tnt <@User> - Blows another user up with TNT"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onTntCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onTntCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            mentioned.asMention + " you have been blown up by " + message.author.asMention + " using TNT! :boom:"
        } else {
            message.jda.selfUser.toString() + " has blown " + message.author.asMention + " up using TNT! :boom:"
        }
    }

    private class Kiss : BaseCommand{
        override val commandName: String
            get() = "kiss"
        override val helpMessage: String
            get() = "kiss <@User> - Kisses another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onKissCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onKissCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            mentioned.asMention + " you have been kissed by " + message.author.asMention + ":lips:"
        } else {
            message.jda.selfUser.toString() + " has slapped you upside the head! :clap:"
        }
    }
    private class Hug : BaseCommand{
        override val commandName: String
            get() = "hug"
        override val helpMessage: String
            get() = "hug <@User> - Hugs another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onHugCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onHugCommand(message: Message, mentioned: Member): String {
        return mentioned.asMention + " you have been hugged by " + message.author.asMention + ":hugging:"
    }
    private class Poke : BaseCommand{
        override val commandName: String
            get() = "poke"
        override val helpMessage: String
            get() = "poke <@User> - Pokes another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onPokeCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onPokeCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            mentioned.asMention + " you have been poked by " + message.author.asMention + ":point_right: "
        } else {
            "You can't poke a bot " + message.author.asMention + "!"
        }
    }
    private class Respect : BaseCommand{
        override val commandName: String
            get() = "rip"
        override val helpMessage: String
            get() = "rip <@User> - Allows you to pay respects"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onPayRespects(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onPayRespects(message: Message, mentioned: Member?): String { // /f command.
        return message.author.asMention.toString() + " pays their respects. https://cdn.discordapp.com/emojis/294160585179004928.png"
    }

    private class BanHammer : BaseCommand{
        override val commandName: String
            get() = "banhammer"
        override val helpMessage: String
            get() = "banhammer <@User> - Swings the banhammer at a user. May or may not miss."
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onBanHammer(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onBanHammer(message: Message, mentioned: Member): String {
        return message.author.asMention + " picks up the <:blobhammer:228112197237080066> and prepares to swing it at " + mentioned.asMention + "! It misses " + mentioned.asMention + " by a hair and they live to see another day!"
    }

    private class Shoot : BaseCommand{
        override val commandName: String
            get() = "shoot"
        override val helpMessage: String
            get() = "shoot <@User> - Shoots another another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onShootCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onShootCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            message.author.asMention+ " picks up a gun and shoots " + mentioned + "! "
        } else {
            "How dare you try to shoot me! *takes out rocket launcher* You shall pay! :boom:"
        }

    }

    private class Stab : BaseCommand{
        override val commandName: String
            get() = "stab"
        override val helpMessage: String
            get() = "stab <@User> - Stabs another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onStabCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onStabCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            mentioned.asMention + " was attacked by " + message.author.asMention + " using a knife!"
        } else {
            "\"breaks knife like a toothpick\" \n \"takes out sword\" \n \"slices " + message.author.asMention + " in half\" \n GET REKT!!! (John Cena Music plays)"
        }


    }

    private class Punch : BaseCommand{
        override val commandName: String
            get() = "punch"
        override val helpMessage: String
            get() = "punch <@User> - Punches another user"
        override val hasOutput: Boolean
            get() = false
        override fun init(message: Message, args: List<String>): String {
            return onPunchCommand(message, JDAUtils.getMentionedUser(message)!!)
        }
    }

    fun onPunchCommand(message: Message, mentioned: Member): String {
        return if (mentioned.user !== message.jda.selfUser) {
            mentioned.asMention + ", you have been punched by " + message.author.asMention + "! :punch:"
        } else {
            "\"ducks aside\""
        }
    }
}
