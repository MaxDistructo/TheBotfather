package maxdistructo.discord.bots.botfather.commands.standard

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent

class Clear : Command(){
    init{
        this.name = "clear"
        this.help = "clear - Clears all but pinned messages in any channel"
        //this.requiredRole = "High Council"
        this.guildOnly = true
    }

    override fun execute(event: CommandEvent?) {
        val message = event!!.message
        for(i in message.channel.iterableHistory.complete(true)){
            if(!i.isPinned){
                i.delete().submit()
            }
        }
    }
}