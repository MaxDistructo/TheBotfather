package maxdistructo.discord.bots.botfather.commands.mafia.data

object Classification {
    const val TS = "Town Support (TS)"
    const val TI = "Town Investigative (TI)"
    const val TK = "Town Killing (TK)"
    const val TP = "Town Protective (TP)"
    const val NK = "Neutral Killing (NK)"
    const val NE = "Neutral Evil (NE)"
    const val NB = "Neutral Benign (NB)"
    const val NC = "Neutral Chaos (NC)"
    const val MK = "Mafia Killing (MK)"
    const val MD = "Mafia Deception (MD)"
    const val MS = "Mafia Support (MS)"
    const val CE = "Coven Evil (CE)"
}