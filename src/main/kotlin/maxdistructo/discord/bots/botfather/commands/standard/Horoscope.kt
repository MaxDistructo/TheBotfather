package maxdistructo.discord.bots.botfather.commands.standard

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.http.exceptions.UnirestException
import maxdistructo.discord.bots.botfather.background.PrivUtils
import maxdistructo.discord.bots.botfather.commands.Base
import maxdistructo.discord.core.jda.message.Messages
import org.json.JSONObject
import net.dv8tion.jda.api.entities.Message

class Horoscope : Command() {

    init{
        this.name = "horoscope"
        this.help = "horoscope <AstralSign> - Gets the horoscope for the provided astral sign."
    }

    override fun execute(event: CommandEvent) {
        event.reply(Messages.simpleEmbed(event.member, onHoroscopeCommand(
                event.message.contentRaw.split(" ")),event.message.guild))
    }

    fun onHoroscopeCommand(args: List<String>): String { // !horoscope <AstralSign>
        val signIn = args[1].toLowerCase() //If command parameters above is met, this will be the astral sign.
        var signOut: String? = null
        when (signIn) {
            "aquarius" -> signOut = "Aquarius"
            "aries" -> signOut = "Aries"
            "taurus" -> signOut = "Taurus"
            "gemini" -> signOut = "Gemini"
            "cancer" -> signOut = "Cancer"
            "leo" -> signOut = "Leo"
            "virgo" -> signOut = "Virgo"
            "libra" -> signOut = "Libra"
            "scorpio" -> signOut = "Scorpio"
            "sagittarius" -> signOut = "Sagittarius"
            "capricorn" -> signOut = "Capricorn"
            "pisces" -> signOut = "Pisces"
            "sagitarius" -> signOut = "Sagittarius" //Cause I typo
        }
        var horoscope: JSONObject? = null
        try {
            horoscope = Unirest.get("http://horoscope-api.herokuapp.com/horoscope/today/" + signOut!!).asJson().body.`object`
        } catch (e: UnirestException) {
            e.printStackTrace()
        }

        return if (horoscope != null) {
            "The horoscope for $signOut is: " + horoscope.getString("horoscope").replace("]".toRegex(),
                    "").replace('[', ' ').replace('\'', ' ')
            //Removes unnessesary characters in API json return.
        } else {
            "Your horoscope has been clouded out of my vision at this time. (API Error)"
        }

    }
}
