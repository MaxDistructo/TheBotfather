package maxdistructo.discord.bots.botfather.commands.mafia.generator

import maxdistructo.discord.bots.botfather.commands.mafia.data.Alignments
import maxdistructo.discord.bots.botfather.commands.mafia.data.Classification
import maxdistructo.discord.bots.botfather.commands.mafia.data.JSONStruct
import maxdistructo.discord.bots.botfather.commands.mafia.data.Roles
import maxdistructo.discord.bots.botfather.commands.mafia.init.IAlignment
import maxdistructo.discord.bots.botfather.commands.mafia.init.IRole
import maxdistructo.discord.bots.botfather.commands.mafia.methods.MafiaConfig
import java.lang.IllegalArgumentException
import kotlin.random.Random

object RoleGenerator {
    fun generateFromList(roleList: List<String>): List<Pair<JSONStruct.UserData, IRole>>{
        val outputList = mutableListOf<Pair<JSONStruct.UserData, IRole>>()
        val playerList = MafiaConfig.getPlayers().shuffled(Random(System.currentTimeMillis()))
        var verify = false
        while(!verify) {
            for ((i, role) in roleList.withIndex()) {
                outputList.add(Pair(playerList[i]!!, translateRoleListEntry(role)))
            }
            verify = verifyRoleList(outputList)
        }
        return outputList
    }
    private fun verifyRoleList(roleList: List<Pair<JSONStruct.UserData, IRole>>): Boolean{
        val uniqueRoleMap = mutableMapOf<IRole, Int>()
        for(role in roleList){
            uniqueRoleMap[role.second] = uniqueRoleMap.getOrDefault(role.second, 0) + 1
        }
        for(value in uniqueRoleMap){
            if(value.key.isUnique && value.value > 1){
                return false
            }
        }
        return true
    }
    fun translateRoleListEntry(role: String): IRole{
        return when (role.toLowerCase()) {
            "ts" -> getRandomRole(Classification.TS)
            "ti" -> getRandomRole(Classification.TI)
            "tp" -> getRandomRole(Classification.TP)
            "tk" -> getRandomRole(Classification.TK)
            "md" -> getRandomRole(Classification.MD)
            "ms" -> getRandomRole(Classification.MS)
            "mk" -> getRandomRole(Classification.MK)
            "ne" -> getRandomRole(Alignments.NEUTRAL_EVIL)
            "nb" -> getRandomRole(Alignments.NEUTRAL_BENIGN)
            "nk" -> getRandomRole(Alignments.NEUTRAL_KILLING)
            "nc" -> getRandomRole(Alignments.NEUTRAL_CHAOS)
            "ce" -> getRandomRole(Classification.CE)
            "rm" -> getRandomRole(Alignments.MAFIA)
            "rt" -> getRandomRole(Alignments.TOWN)
            "rn" -> getRandomRole("neutral")
            "any" -> getRandomRole("any")
            else -> getRole(role)
        }
    }
    fun getRole(role: String): IRole {
        try {
            for(roleL in Roles.values()){
                if(roleL.regName == role){
                    return roleL
                }
            }
        }
        catch(e: IllegalArgumentException){
            return Roles.ADMIN
        }
        return Roles.ADMIN
    }
    fun getRandomRole(classification: String): IRole{
        return getRoles(classification, 1).shuffled(Random(System.currentTimeMillis()))[0]
    }
    fun getRandomRole(alignment: IAlignment): IRole{
        return getRoles(alignment, 2).shuffled(Random(System.currentTimeMillis()))[0]
    }
    //As there is a limited amount of type int supported, the function is private to prevent issues with improper calls
    private fun getRoles(value: Any, type: Int): List<IRole>{
        val output = mutableListOf<IRole>()
        for(role in Roles.values()){
            if(type == 1 && value == "neutral"){ //Special Case 1: Neutrals need to self call for all 4 types
                output.addAll(getRoles(Alignments.NEUTRAL_BENIGN, 2))
                output.addAll(getRoles(Alignments.NEUTRAL_EVIL, 2))
                output.addAll(getRoles(Alignments.NEUTRAL_KILLING,2))
                output.addAll(getRoles(Alignments.NEUTRAL_CHAOS, 2))
            }
            else if(type == 1 && value == "any"){ //Special Case 2: any should add all roles and return that.
                output.addAll(Roles.values())
            }
            else if(type == 1) {
                if (role.classification == value as String) {
                    output.add(role)
                }
            }
            else if(type == 2){
                if(role.alignment == value as IAlignment){
                    output.add(role)
                }
            }
        }
        return output
    }

}