package maxdistructo.discord.bots.botfather.commands.mafia.init

import net.dv8tion.jda.api.entities.Message


interface IHandler  {
    val name : String
    fun update(message : Message)
    fun reset(message : Message)
}