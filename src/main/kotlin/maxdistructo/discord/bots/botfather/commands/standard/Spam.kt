package maxdistructo.discord.bots.botfather.commands.standard

import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.background.PrivUtils
import maxdistructo.discord.bots.botfather.commands.Base
import maxdistructo.discord.core.jda.Perms
import maxdistructo.discord.core.Utils
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Member

class Spam : Base.BaseCommand() {

    override val commandName: String
        get() = "spam"
    override val helpMessage: String
        get() = "spam <@User> NumberOfTimes - Spams a user in their DMs."
    override val requiresMod: Boolean
        get() = true
    override val hasOutput: Boolean
        get() = false

    override fun init(message: Message, args: List<String>): String {
        return onSpamCommand(PrivUtils.listToArray(args) as Array<Any>, message, JDAUtils.getMentionedUser(message)!!)
    }

    fun onSpamCommand(args: Array<Any>, message: Message, mentioned: Member): String {
        //Spams a user in their DMs Command: prefix + spam <@User> NumberOfTimes
        val author = message.member
        var spamPlayer = mentioned
        val spamNum: Int
        if (args.size == 3) {
            spamNum = Integer.valueOf(args[2].toString())
        } else {
            return "You did not enter enough arguments to run this command."
        }
        if (mentioned.user === BaseBot.client.selfUser && !Perms.checkMod(message)) {
            spamPlayer = author!!
        }
        var i = 0
        while (i < spamNum) {
            Messages.sendDM(spamPlayer.user, "YOU GOT SPAMMED SON!", false)
            i++
        }
        return "Player $spamPlayer will be spammed $spamNum times."

    }
}
