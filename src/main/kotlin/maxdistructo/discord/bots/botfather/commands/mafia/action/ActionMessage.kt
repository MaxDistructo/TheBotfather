package maxdistructo.discord.bots.botfather.commands.mafia.action

import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.commands.mafia.data.JSONStruct
import maxdistructo.discord.core.jda.message.Messages

object ActionMessage {
    enum class Actions{
        HEAL,
        ATTACKED,
        TRANSPORTED,
        CONVERTED,
        IMMUNE,
        REMEMBERED,
        DNR,
        HEALED,
        RBED,
        NULL
    }
    fun getMessage(action : Enum<Actions>, user : JSONStruct.UserData){
        when(action){
            Actions.ATTACKED -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You were attacked last night!")
            Actions.REMEMBERED -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You have remembered who you were.")
            Actions.CONVERTED -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You have been converted to a Vampire!")
            Actions.IMMUNE -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "Your target was immune to your attack!")
            Actions.DNR -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "Your target did not require healing.")
            Actions.RBED -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You were roleblocked last night.")
            Actions.TRANSPORTED -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You were transported")
            Actions.HEAL -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You were attacked and healed")
            Actions.HEALED -> Messages.sendDM(BaseBot.client.getUserById(user.id)!!, "You have successfully healed your target.")
        }
    }
}