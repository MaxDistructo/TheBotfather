package maxdistructo.discord.bots.botfather.commands.mafia.init

import java.awt.Color
import java.net.URL

interface IRole {
    val regName : String
    val isUnique: Boolean
    val classification : String
    val alignment : IAlignment
    val summary : String
    val description : String
    val thumbnail : String
    val icon : String
    val attack : Int
    val defence : Int
    fun getColor(): Color
}