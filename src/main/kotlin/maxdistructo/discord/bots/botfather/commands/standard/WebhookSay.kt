package maxdistructo.discord.bots.botfather.commands.standard

import maxdistructo.discord.bots.botfather.commands.Base
import maxdistructo.discord.core.Utils
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Webhook

import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.TextChannel

class WebhookSay : Base.BaseCommand() {
    override val commandName: String
        get() = "webhook"
    override val helpMessage: String
        get() = "webhook @User <message> - Says the following message as the user"
    override val requiresMod: Boolean
        get() = true
    override val hasOutput : Boolean
        get() = true

    override fun init(message: Message, args: List<String>): String {
        onWebhookSay(args, message, JDAUtils.getMentionedChannel(message) as TextChannel?)
        return ""
    }

    private fun onWebhookSay(args: List<Any>, message : Message, mentionedChannel: TextChannel?){
        val user = JDAUtils.getUserFromInput(message, args[1])
        var anotherChannel = false
        val output : String
        if(mentionedChannel != null){
            anotherChannel = true
        }
        if(anotherChannel){
            output = Utils.makeNewString(args, 3)
            Webhook.send(mentionedChannel!!, user!!.effectiveName, user.user.avatarUrl!!, output )
        }
        else{
            output = Utils.makeNewString(args, 2)
            Webhook.send(message.textChannel, user!!.effectiveName, user.user.avatarUrl!!, output)
        }
    }


}