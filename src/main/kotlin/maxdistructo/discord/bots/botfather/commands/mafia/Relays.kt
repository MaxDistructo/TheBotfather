package maxdistructo.discord.bots.botfather.commands.mafia

import maxdistructo.discord.bots.botfather.commands.mafia.data.Alignments
import maxdistructo.discord.bots.botfather.commands.mafia.data.Roles
import maxdistructo.discord.bots.botfather.commands.mafia.methods.MafiaConfig
import maxdistructo.discord.core.jda.Config
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter


class Relays : ListenerAdapter() {
    override fun onPrivateMessageReceived(event: PrivateMessageReceivedEvent) {
        if (!event.message.contentRaw.split(" ")[0].contains(Config.readPrefix())) {
            if(MafiaConfig.checkDay()) { //TODO Implement Dead Check
                for (player in MafiaConfig.getPlayers()) {
                    if (event.author.idLong != player!!.id && player.alive) {
                        val user = event.jda.getUserById(player.id)!!
                        Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                + event.message.contentDisplay)
                    }
                }
            }
            else{
                val player = MafiaConfig.getPlayer(event.author)
                val isMafia = player!!.role.alignment == Alignments.MAFIA
                val isCoven = player.role.alignment == Alignments.COVEN
                val isVampire = player.role == Roles.VAMPIRE
                val isJailed = player.jailed
                val isJailor = player.role == Roles.JAILOR
                val isMedium = player.role == Roles.MEDIUM
                val isDead = !player.alive
                val user = event.jda.getUserById(player.id)!!
                for(player2 in MafiaConfig.getPlayers()){
                    if(player2!!.id != player.id) {
                        if (isMafia && player2.role.alignment == Alignments.MAFIA) {
                            Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                    + event.message.contentDisplay)
                        }
                        if (isCoven && player2.role.alignment == Alignments.COVEN) {
                            Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                    + event.message.contentDisplay)
                        }
                        if (isVampire && player2.role == Roles.VAMPIRE) {
                            Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                    + event.message.contentDisplay)
                        }
                        if (isJailed && player2.role == Roles.JAILOR) {
                            Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                    + event.message.contentDisplay)
                        }
                        if (isJailor && player2.jailed){
                            Messages.sendDM(user, "Jailor: "
                                    + event.message.contentDisplay)
                        }
                        if (isMedium && !player2.alive && !isDead && !player2.jailed){
                            Messages.sendDM(user, "Medium: "
                                    + event.message.contentDisplay)
                        }
                        if (isDead && player2.alive && player2.role == Roles.MEDIUM){
                            Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                    + event.message.contentDisplay)
                        }
                        if (isDead && isMedium && !player2.alive){
                            Messages.sendDM(user, "${event.author.name}#${event.author.discriminator}: "
                                    + event.message.contentDisplay)
                        }
                    }
                }
            }
        }
    }
}