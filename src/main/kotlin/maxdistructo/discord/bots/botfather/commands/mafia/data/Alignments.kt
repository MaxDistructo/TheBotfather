package maxdistructo.discord.bots.botfather.commands.mafia.data

import maxdistructo.discord.bots.botfather.commands.mafia.init.IAlignment
import java.awt.Color

enum class Alignments: IAlignment {
    TOWN{
        override val regName: String = "town"
        override val displayName: String = "Town"
        override val color: Color = Color( 0x6BC617)
        override val allies = "Town\nSurvivors"
        override val mustKill = "The Mafia, The Coven, and all Neutral Killers"
    },
    MAFIA{
        override val regName: String = "mafia"
        override val displayName: String = "Mafia"
        override val color: Color = Color(0xDF0205)
        override val allies = "Mafia\nSurivors"
        override val mustKill = "Anyone who would oppose you"
    },
    COVEN{
        override val regName: String = "coven"
        override val displayName: String = "Coven"
        override val color: Color = Color(0xC15FFF) //Change Me to Coven Color
        override val allies = "Coven\nSurivors"
        override val mustKill = "Anyone who would oppose you"
    },
    NEUTRAL_EVIL{
        override val regName: String = "neutral_evil"
        override val displayName: String = "Neutral Evil"
        override val color: Color = Color(0xFFFFFF)
        override val allies = "Anyone who will help you achieve your goal"
        override val mustKill = "Nobody"
    },
    WITCH_NE{
        override val regName: String = "neutral_evil"
        override val displayName: String = "Neutral Evil"
        override val color: Color = Color(0xFFFFFF)
        override val allies = "Mafia\nSurvivors"
        override val mustKill = "Town"
    },
    NEUTRAL_BENIGN{
        override val regName: String = "neutral_benign"
        override val displayName: String = "Neutral Benign"
        override val color: Color = Color(0xFFFFFF)
        override val allies = "Anyone who will help you achieve your goal"
        override val mustKill = "Nobody"
    },
    NEUTRAL_KILLING{
        override val regName: String = "neutral_killing"
        override val displayName: String = "Neutral Killing"
        override val color: Color = Color(0xFFFFFF)
        override val allies = "None"
        override val mustKill = "Anyone who would oppose you"
    },
    NEUTRAL_CHAOS{
        override val regName: String = "neutral_chaos"
        override val displayName: String = "Neutral Chaos"
        override val color: Color = Color(0x000000)
        override val allies = "None"
        override val mustKill = "Anyone who would oppose you"
    },
    NULL{
        override val regName: String = "null"
        override val displayName: String = "null"
        override val color: Color = Color(0x000000)
        override val allies = "None"
        override val mustKill = "None"
    }
}