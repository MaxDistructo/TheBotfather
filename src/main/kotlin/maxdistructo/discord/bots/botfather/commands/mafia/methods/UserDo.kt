package maxdistructo.discord.bots.botfather.commands.mafia.methods

//import maxdistructo.discord.bots.botfather.commands.mafia.MafiaListener
import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.commands.mafia.obj.Details
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.entities.Message

object UserDo {

    fun message(message: Message, messageContent: Array<Any>) {
        val player = MafiaConfig.getPlayer(message.author)!!
        val mentioned = Mafia.getUserFromInput(message, messageContent[2])!!
        when (player.role.regName){
            "mafioso" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " has voted to kill " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You have voted to kill " + mentioned.effectiveName)
                message.delete().complete()
            }
            "godfather" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " has decided to kill " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You have decided to kill " + mentioned.effectiveName)
                message.delete().complete()
            }
            "serial_killer" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is gonna go and stab " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You have decided to go and stab " + mentioned.effectiveName)
                message.delete().complete()
            }
            "werewolf" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is gonna go and visit " + mentioned.effectiveName)
                if(mentioned.effectiveName.toCharArray().last() != 's') {
                    Messages.sendDM(message.author, "You have decided to go and rampage at " + mentioned.effectiveName + "'s house")
                }
                else{
                    val charArray = mentioned.effectiveName.toCharArray().slice(0 until (mentioned.effectiveName.toCharArray().size - 1))
                    Messages.sendDM(message.author, "You have decided to go and rampage at $charArray's house")
                }
                message.delete().complete()
            }
            "arsonist" -> {
                if (mentioned !== message.author) {
                    Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is gonna douse " + mentioned.effectiveName)
                    Messages.sendDM(message.author, "You have decided to douse " + mentioned.effectiveName)
                } else {
                    Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is gonna set all doused players on fire")
                    //Messages.sendDM(message.author, "You have decided to set all targets on fire tonight")
                }
                message.delete().complete()
            }
            "jester" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is gonna haunt " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You have decided to haunt " + mentioned.effectiveName)
                message.delete().complete()
            }
            "veteran" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is going on alert tonight.")
                Messages.sendDM(message.author, "You have decided to go on alert tonight")
                message.delete().complete()
            }
            "vigilante" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " is going to shoot " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You have decided to shoot " + mentioned.effectiveName)
                message.delete().complete()
            }
            "jailor" -> {
                if(MafiaConfig.checkDay()){
                    Messages.sendDM(message.author, "You will be jailing " + mentioned.effectiveName + " tonight.")
                }
                else{
                    if(!MafiaConfig.getExecute()) {
                        Messages.sendDM(message.author, "You will be executing " + BaseBot.client.getUserById(MafiaConfig.getJailed()!!.id)!!)
                    }
                    else{
                        Messages.sendDM(message.author, "You have decided against executing " + BaseBot.client.getUserById(MafiaConfig.getJailed()!!.id)!!)
                    }
                    MafiaConfig.toggleExecute()
                }
                message.delete().complete()
            }
            "vampire" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, "The vampires are going to try and convert " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You will be biting " + mentioned.effectiveName)
                message.delete().complete()
            }
            "vampire_hunter" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " will be checking and stabbing " + mentioned.effectiveName + " if they are a vampire")
                Messages.sendDM(message.author, "You will be checking " + mentioned.effectiveName)
                message.delete().complete()
            }
            "lookout" -> {
                Messages.sendDM(message.author, "You will be watching " + mentioned.effectiveName + " tonight.")
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " will be watching " + mentioned.effectiveName + " tonight.")
                message.delete().complete()
            }
            "investigator" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " would like to investigate " + mentioned.effectiveName + " tonight.")
                Messages.sendDM(message.author, "You are going to investigate " + mentioned.effectiveName)
                message.delete().complete()
            }
            "sheriff" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + "would like to interrogate " + mentioned.effectiveName + " tonight.")
                Messages.sendDM(message.author, "You are going to be interrogating " + mentioned + " tonight.")
                message.delete().complete()
            }
            "transporter" -> {
                val target = Mafia.getUserFromInput(message, messageContent[2])
                val invest = Mafia.getUserFromInput(message, messageContent[3], 1)
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " would like to swap positions of " + invest!!.effectiveName + " & " + target!!.effectiveName)
                Messages.sendDM(message.author, "You will be transporting " + invest.effectiveName + " & " + target!!.effectiveName)
                message.delete().complete()
            }
            "witch" -> {
                val target = Mafia.getUserFromInput(message, messageContent[2])
                val invest = Mafia.getUserFromInput(message, messageContent[3], 1)
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " would like to control " + invest!!.effectiveName + " into using their ability onto " + target!!.effectiveName)
                Messages.sendDM(message.author, "You will be witching " + invest.effectiveName + " into " + target!!.effectiveName)
                message.delete().complete()
            }
            "doctor" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " will be healing " + mentioned.effectiveName + " tonight.")
                Messages.sendDM(message.author, "You will be healing " + mentioned.effectiveName + " tonight.")
                message.delete().complete()
            }
            "bodyguard" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " will be guarding " + mentioned.effectiveName + " tonight.")
                Messages.sendDM(message.author, "You will be guarding " + mentioned.effectiveName + " tonight.")
                message.delete().complete()
            }
            "escort" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " would like to roleblock " + mentioned + " tonight.")
                Messages.sendDM(message.author, "You will be escorting " + mentioned.effectiveName + " tonight.")
                message.delete().complete()
            }
            "consort" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " would like to roleblock " + mentioned + " tonight.")
                Messages.sendDM(message.author, "You will be escorting " + mentioned.effectiveName + " tonight.")
                message.delete().complete()
            }
            "mayor" -> {
                if (MafiaConfig.checkDay()) {
                    Mafia.broadcast(message.member!!.effectiveName + " has revealed themselves as the Mayor!")
                    MafiaConfig.editDetails(message.member!!.idLong, Details.REVEALED, true)
                    Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " has revealed as the mayor. Their votes now count as 3.")
                }
                message.delete().complete()

            }
            "medium" -> {
                if (MafiaConfig.checkDay() && !player.alive) {
                    Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.member!!.effectiveName + " would like to talk to " + mentioned.effectiveName)
                    Messages.sendDM(message.author, "Your message has been sent to the Admin. Please wait for them to respond to your secance request")
                    message.delete().complete()
                }
            }
            "retributionist" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " will be reviving " + mentioned + " tonight.")
                Messages.sendDM(message.author, "You will be reviving " + mentioned.effectiveName + " tonight.")
                message.delete().complete()
            }
            "disguiser" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " is going to be disguised as " + mentioned.effectiveName + " in role.")
                MafiaConfig.editDetails(message.author.idLong, Details.DISGUISED, MafiaConfig.getPlayer(mentioned.idLong)!!.role )
                Messages.sendDM(message.author, "You will be disguising as " + mentioned.effectiveName)
                message.delete().complete()
            }
            "forger" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " is gonna forge the role of " + mentioned.effectiveName + " to be Forger.")
                Messages.sendDM(message.author, "You will be forging the role of " + mentioned.effectiveName)
                message.delete().complete()
            }
            "framer" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " is gonna frame " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You will be framing " + mentioned.effectiveName)
                message.delete().complete()
            }
            "janitor" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " would like to clean the role of " + JDAUtils.getMentionedUser(message)!!.effectiveName)
                Messages.sendDM(message.author, "You will be cleaning " + mentioned.effectiveName)
                message.delete().complete()
            }
            "blackmailer" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + "would like to Blackmail " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You will be blackmailing " + mentioned.effectiveName)
                message.delete().complete()
            }
            "consigerge" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " would like to know the role of " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You will receive the role of " + mentioned.effectiveName)
                message.delete().complete()
            }
            "amnesiac" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " would like to remember the role of " + mentioned.effectiveName)
                Messages.sendDM(message.author, "You will remember the role of " + mentioned.effectiveName)
                message.delete().complete()
            }
            "survivor" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " will be putting on a vest tonight.")
                Messages.sendDM(message.author, "You will be putting on a vest tonight.")
                message.delete().complete()
            }
            "tracker" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " will be tracking " + mentioned.effectiveName + " tonight.")
                Messages.sendDM(message.author, "You will be tracking " + mentioned.effectiveName)
                message.delete().complete()
            }
            "trapper" -> {
                Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, message.author.name + " will be placing a trap at " + mentioned.effectiveName + "'s house tonight.")
                Messages.sendDM(message.author, "You will be placing a trap at " + mentioned.effectiveName + "'s house.")
                message.delete().complete()
            }

        }


    }
}
