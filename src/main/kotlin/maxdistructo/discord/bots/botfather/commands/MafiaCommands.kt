package maxdistructo.discord.bots.botfather.commands

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.background.PrivUtils
import maxdistructo.discord.bots.botfather.commands.mafia.data.Roles
import maxdistructo.discord.bots.botfather.commands.mafia.generator.RoleGenerator
import maxdistructo.discord.bots.botfather.commands.mafia.methods.*
import maxdistructo.discord.bots.botfather.commands.mafia.obj.Details
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import maxdistructo.discord.core.jda.message.Webhook


object MafiaCommands {
    class MafiaBaseCommand : Command(){
        override fun execute(event: CommandEvent) {
            event.replyInDm("Please use a mafia subcommand.")
        }
        init{
            this.name = "mafia"
            this.help = "Mafia Game Commands"
            this.arguments = argBuilder()
            this.children = arrayOf(Do(), Join(), Continue(), RoleCard(), KillCommand(), JailPlayer(), Vote(),
                    ModInfo(), Start(), Fixer(), TopicFixer(), Revive(), Info(), SetRole())
        }
        private fun argBuilder() : String{
            val builder = StringBuilder()
            for (value in this.getChildren()){
                builder.append(value.name)
                builder.append(",")
            }
            return builder.toString()
        }
    }

    class Do : Command(){
        init {
            this.name = "action"
            this.help = "Uses your action on the provided user. Day/Night"
            this.arguments = "<User>"
            this.guildOnly = true
        }
        override fun execute(event: CommandEvent) {
            UserDo.message(event.message, PrivUtils.listToArray(event.message.contentDisplay.split(" ")) as Array<Any>)
        }
    }

    class Join : Command(){
        init {
            this.name = "join"
            this.help = "Adds you to the next mafia game"
            this.guildOnly = true
        }
        override fun execute(event: CommandEvent) {
            Mafia.onGameJoinCommand(event.message)
        }
    }
    class Continue : Command(){
        init{
            this.name = "continue"
            this.help = "Continues the Mafia Game"
            this.requiredRole = "Mafia Admin"
            this.guildOnly = true
        }
        override fun execute(event: CommandEvent?) {
            Mafia.onGameToggle(event!!.message)
        }
    }
    class Start : Command(){
        init{
            this.name = "start"
            this.help = "mafia start - Starts the mafia game with all joined players"
            this.guildOnly = true
            this.requiredRole = "Mafia Admin"
        }
        override fun execute(event: CommandEvent?) {
            Mafia.onGameStart(event!!.message)
        }
    }
    class Info : Command(){
        init{
            this.name = "info"
            this.help = "mafia info - Gets your info"
            this.guildOnly = true
        }

        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            val details = MafiaConfig.getPlayer(message.author)
            Messages.sendDM(message.author, Messages.simpleEmbed(message.member!!, "Mafia Details\n" +
                    "Alignment: " + details!!.role.alignment + "\n" + "Role: " + details.role.regName + "\n" +
                            "Attack: " + details.attack + "\nDefence: " + details.defence, message))
        }
    }
    class ModInfo : Command(){
        init{
            this.name = "getInfo"
            this.help = "mafia getInfo - Gets info on the specified user"
            this.requiredRole = "Mafia Admin"
            this.guildOnly = true
        }

        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            val args = message.contentDisplay.split(" ")
            //val details = MafiaConfig.getPlayerDetails(message, Mafia.getUserFromInput(message, args[2])!!.user.idLong)
            //TODO(FIX ME) Messages.sendDM(message.author, Messages.simpleEmbed(Mafia.getUserFromInput(message, args[2])!!, "Mafia Details","Alignment: " + details[0] + "\nClass: " + details[1] + "\nRole: " + details[2] + "\nIs Dead: " + details[3] + "\nAttack Power: " + details[4] + "\nDefence Power: " + details[5], message))
        }
    }
    class RoleCard : Command(){
        init{
            this.name = "rolecard"
            this.help = "Shows the rolecard for a specified role"
            this.guildOnly = true
        }
        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            val args = event.event.message.contentDisplay.split(" ")
            Messages.sendMessage(message.textChannel, RoleCards.generate(RoleGenerator.getRole(args[2])))
        }
    }
    class SetRole : Command(){
        init{
            this.name = "setrole"
            this.help = "mafia setrole <User> <RoleName> - Sets the role of the provided user to the specified role"
            this.guildOnly = true
        }

        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            val args = message.contentRaw.split(" ")
            val mentioned = JDAUtils.getUserFromInput(message, args[2])!!.user
            Messages.sendDM(message.author, "The role of " + mentioned.name + " has been set to ")
            MafiaConfig.editDetails(mentioned.idLong, Details.ROLE, Roles.valueOf(args[3]))
            Messages.sendDM(JDAUtils.getUserFromInput(message, args[2])!!.user, "Your role has been set to " +
                    args[3] + " due to either Unique role or specific role change.")
        }
    }

    class KillCommand : Command(){
        init{
            this.name = "kill"
            this.help = "Kills the provided user"
            this.arguments = "<User> <killer|-2kill killer1 killer2|-3kill killer1 killer2 killer3|-clean>"
            this.guildOnly = true
        }
        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            val args = event.event.message.contentDisplay.split(" ")
            MafiaConfig.editDetails(JDAUtils.getUserFromInput(message, args[2])!!.idLong, Details.ALIVE, false)
            for(player in MafiaConfig.getPlayers()){
                Messages.sendDM(BaseBot.bot.client.getUserById(player!!.id)!!,
                        Kill.message(message, PrivUtils.listToArray(args) as Array<Any>))
            }
        }
    }

    class JailPlayer : Command(){
        init{
            this.name = "jail"
            this.help = "Jails the provided user"
            this.arguments = "<User>"
            this.guildOnly = true
        }
        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            val args = event.event.message.contentDisplay.split(" ")
            val player = MafiaConfig.getPlayer(event.author.idLong)!!
            if(player.role.regName == "jailor") {
                if (JDAUtils.getUserFromInput(message, args[2])!! === message.member!!) {
                    Messages.sendDM(message.author, "You can not jail yourself!")
                }
                else {
                    Mafia.jailPlayer(message, JDAUtils.getUserFromInput(message, args[2])!!)
                    Messages.sendDM(message.author, "You will be Jailing " + JDAUtils.getUserFromInput(message, args[2])!!.effectiveName)
                }
            }
            else{
                Messages.sendDM(message.author, "You attempted to drag " + JDAUtils.getUserFromInput(message, args[2])!!.effectiveName + " to the Jail but they fought you off.")
            }
            message.delete()
        }
    }

    class Vote : Command(){
        init {
            this.name = "vote"
            this.help = "Votes for the specified user"
            this.arguments = "<User>"
            this.guildOnly = true
        }

        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            /*
            val player = Player(MafiaConfig.getPlayerDetails(message))
            if (player.roleString == "mayor" ) {
                Messages.sendMessage(message.channel, message.member!!.toString() + " has voted for " + JDAUtils.getMentionedUser(message)!!.asMention)
            }
            else {
                Messages.sendMessage(message.channel, message.member!!.toString() + " has voted for " + JDAUtils.getMentionedUser(message)!!.asMention)
            }
            */

            message.delete()
        }
    }
    /*
        class AdminMessage : MafiaCommand(){ //!mafia message @user message
            override val commandName: String
                get() = "message"
            override val requiresMod: Boolean
                get() = true
            override val helpMessage: String
                get() = "mafia message <User> <message> - Sends an official message to the provided user through the bot. (Only use for non provided messages)"

            override fun init(message: Message, args: List<String>): String {
                Messages.sendDM(JDAUtils.getUserFromInput(message, args[2])!!.user, Utils.makeNewString(args, 3))
                return ""
            }
        }

        class Action : MafiaCommand(){
            override val commandName: String
                get() = "action"
            override val helpMessage: String
                get() = "mafia action <@user> <action>"
            override val requiresMod: Boolean
                get() = true

            override fun init(message: Message, args: List<String>): String { //!mafia action @user action
                ActionMessage.getMessage(EnumSelector.mafiaAction(args[3]), Player(message, JDAUtils.getUserFromInput(message, args[2])!!))
                return ""
            }
        }

        class Generate : MafiaCommand(){
            override val commandName: String
                get() = "generate"
            override val helpMessage: String
                get() = "mafia generate - Generates a mafia playerdat file"
            override val requiresMod: Boolean
                get() = true
            override fun init(message : Message , args : List<String>) : String{
                val out = Mafia.assignRoles(message)
                MafiaConfig.writeGame(message, out.first)
                return ""
            }
        }

        class Leave : MafiaCommand(){
            override val commandName: String
                get() = "leave"
            override val helpMessage: String
                get() = "mafia leave - Removes you from the mafia games"

            override fun init(message: Message, args: List<String>): String {
                Mafia.onGameLeaveCommand(message)
                return ""
            }
        }
    */
    class Fixer : Command(){
        init{
            this.name = "fix"
            this.help = "mafia fix - Fixes role and channel permissions for users"
            this.guildOnly = true
            this.requiredRole = "Mafia Admin"
        }

        override fun execute(event: CommandEvent?) {
            val message = event!!.message
            Mafia.permFixer(message)
            Messages.sendMessage(message.textChannel, "Fixing permissions and roles for all players")
        }
    }

    class TopicFixer : Command(){
        init{
            this.name = "topic"
            this.help = "mafia topic - Re-calculates the topic for the day channel"
            this.guildOnly = true
            this.requiredRole = "Mafia Admin"
        }

        override fun execute(event: CommandEvent) {
            //Toggle.updateTopic(event.message, Game(JSONUtils.readJSONFromFile("/config/mafia/" + event.message.guild.idLong + "_dat.txt")))
        }
    }

    class Revive : Command(){
        init{
            this.name = "revive"
            this.help = "mafia revive - Revives the user"
            this.guildOnly = true
            this.requiredRole = "Mafia Admin"
        }

        override fun execute(event: CommandEvent) {
            val message = event.message
            MafiaConfig.editDetails(
                    JDAUtils.getUserFromInput(message, message.contentDisplay.split(" ")[2])!!.idLong,
                    Details.ALIVE, true)
            event.reply("Revived " + JDAUtils.getUserFromInput(message,
                    message.contentDisplay.split(" ")[2])!!.effectiveName)
        }
    }


}