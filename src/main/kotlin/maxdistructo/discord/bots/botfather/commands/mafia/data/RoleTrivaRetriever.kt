package maxdistructo.discord.bots.botfather.commands.mafia.data

import maxdistructo.discord.bots.botfather.commands.mafia.init.IRole
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.io.FileWriter

object RoleTrivaRetriever {
    fun getTrivia(role: IRole): String{
        return getTrivia(role.regName)
    }
    fun getTrivia(role: String): String{
        val url = "https://town-of-salem.fandom.com/wiki/${role}"
        val soup = Jsoup.connect(url).get()
        val triviaElement = soup.select("li")
        val triviaElements = mutableListOf<Element>()
        for(element in triviaElement){
            if(element.toString().toLowerCase().contains("version")){
                triviaElements.add(element)
            }
        }
        return triviaElements.shuffled()[0].text()
    }
}