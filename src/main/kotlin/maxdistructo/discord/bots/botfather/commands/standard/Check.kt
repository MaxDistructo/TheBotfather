package maxdistructo.discord.bots.botfather.commands.standard

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.Permission
import net.dv8tion.jda.api.entities.Member
import java.util.*

class Check : Command(){
    init{
        this.name = "check"
        this.guildOnly = true
        this.help = "Check: Checks your permissions"
    }

    override fun execute(event: CommandEvent) {
        val message = event.message
        val userPermissions: EnumSet<Permission>
        val stringBuilder = StringBuilder()
        val channel = "#" + event.channel.name
        val member: Member
        if(message.mentionedUsers.isNotEmpty()){
            member = JDAUtils.getMentionedUser(message)!!
        }
        else{
           member = event.member
        }
        userPermissions = member.permissions

        stringBuilder.append(member.asMention +
                "'s permissions in channel $channel of server ${event.guild.name} \n")
        userPermissions.forEach{
            permission ->
                stringBuilder.append(permission.getName())
                stringBuilder.append("\n")
            }
        event.reply(Messages.simpleEmbed(message.member!!,stringBuilder.toString(), message.guild))
    }
}
