package maxdistructo.discord.bots.botfather.commands.standard


import maxdistructo.discord.bots.botfather.commands.Base
import net.dv8tion.jda.api.entities.Message

class Game : Base.BaseCommand() {

    override val commandName: String
        get() = "Game"
    override val requiresMod: Boolean
        get() = false
    override val requiresAdmin: Boolean
        get() = false
    override val helpMessage : String
        get() = ""
    override val hasOutput: Boolean
        get() = false

    override fun init(message : Message, args : List<String>) : String {
        return "Command Error: $commandName"
    }
}