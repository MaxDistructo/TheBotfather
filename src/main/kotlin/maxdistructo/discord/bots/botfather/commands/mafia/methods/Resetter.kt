package maxdistructo.discord.bots.botfather.commands.mafia.methods

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import maxdistructo.discord.bots.botfather.commands.mafia.Perms
import maxdistructo.discord.bots.botfather.commands.mafia.obj.Details
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.entities.Message

object Resetter {

    class ResetCommand : Command(){
        init{
            this.name = "reset"
            this.help = "mafia reset - Resets the mafia game."
            this.guildOnly = true
        }

        override fun execute(event: CommandEvent?) {
            reset(event!!.message)
        }
    }

    private fun reset(message : Message){
        Messages.sendMessage(message.channel, "Reset has begun. Please wait.")
        GlobalScope.launch {
            println("Resetting Overrides")
            resetOverrides()
            println("Resetting Channels")
            resetChannels()
            println("Resetting Player Count to 0")
            resetPlayers()
            Messages.sendMessage(message.channel, "Reset has been completed.")
        }
        Messages.sendMessage(message.channel,
                "Resetting Bot. Please wait for finished message before starting another game.")
    }

    private fun resetOverrides(){
        val general = MafiaConfig.getGeneral()
        for(player in MafiaConfig.getPlayers()){
            Perms.removePerms(general, player!!.id)
            Perms.allowReadWrite(general, player.id)
        }
    }

    private fun resetChannels() {
        val channels = listOf(MafiaConfig.getGeneral())
        for (channel in channels) {
            for (message2 in channel.iterableHistory) {
                if (message2 != null) {
                    if (!message2.isPinned) {
                        message2.delete().submit()
                    }
                }
            }
        }
    }

    private fun resetPlayers(){
        for (player in MafiaConfig.getPlayers()){
            MafiaConfig.editDetails(player!!.id, Details.TIER, 0)
        }
    }
}