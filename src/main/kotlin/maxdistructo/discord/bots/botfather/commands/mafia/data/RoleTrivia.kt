package maxdistructo.discord.bots.botfather.commands.mafia.data

import maxdistructo.discord.bots.botfather.commands.mafia.init.IRoleTrivia

enum class RoleTrivia(val regName: String): IRoleTrivia {
    INVESTIGATOR("investigator"){
        override val triviaEntries: List<String> = mutableListOf(
                "Prior to Version 1.5.11.5389, there were different investigative results. They were removed because they were considered unbalanced and made gameplay difficult for certain roles.",
                "Patch 2.2.0.8155 swapped the investigative results for the Witch and Amnesiac so that it was more balanced for a Witch in non-Coven ranked games.",
                "According to a poll made by Naru2008 the Investigator is the fourth most loved role in Town of Salem with 192 votes as their favorite, the main reasoning being that those users loved validating claims and opening (or closing) claim space.",
                "Investigator is receiving a change in a coming update."
        )
    },
    SHERIFF("sheriff"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 2.5.1.10655, the Sheriff could differ between a target being a member of the Mafia, a member of the Coven, a Serial Killer, and a Werewolf on a Full Moon. All of these roles now appear as \"suspicious\". ",
                "Prior to Version 2.5.2.10840, Sheriffs would receive the message \"Your target is not suspicious.\" when interrogating an innocent target, which was replaced with more flavor text, appearing as \"You cannot find evidence of wrongdoing. Your target seems innocent\" as the two were easily mixed up. "
        )
    },
    SPY("spy"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 2.0.0.6537, the Spy and the Blackmailer could read whispers. Now, only the Blackmailer can read whispers.",
                "Prior to Version 2.0.0.6537, the Spy could read the Mafia chat.",
                "As of Version 2.3.8910, Spies will die upon visiting a Veteran on alert.",
                "As of Version 1.5.8.4235, Spies are once again able to be the target of an Executioner."
        )
    },
    LOOKOUT("lookout"){
        override val triviaEntries = mutableListOf(
                "The Lookout is one of the few roles that has not been changed"
        )
    },
    JAILOR("jailor"){
        override val triviaEntries = mutableListOf(
                "If you are killed by the Serial Killer on the first day, there is only a ~52% chance that you have jailed them.",
                "Prior to Version A.0.1.8, the Jailor was not a unique role. ",
                "Prior to Version 2.0, Jailors had Death Notes in the same form as any other killing role. This allowed them to be easily confirmed by revealing who they are in their Death Note. ",
                "Prior to Version 2.10, Jailors had no Death Note. They are now the only Town Killing role with a Death Note and the only role with a different style of Death Note. ",
                "According to a poll made by Naru2008, the Jailor is the most loved role in Town of Salem with had 541 votes, the main reasoning because they can lead the Town to victory. ",
                "The Jailor is considered the most powerful Town role. It is the only Town role with a Unstoppable Attack as well as being the only role in the entire game which can override role block immunity. ",
                "The Jailor is one of four roles that have a day ability, the other being the Mayor , Pirate and a dead Medium. ",
                "The Jailor is the only role in the game that is present in all non-coven game modes, excluding All Any. ",
                "Prior to version 1.5.8.4235, the Jailor was able to be the target of an Executioner."
        )
    },
    VAMP_HUNTER("vampire_hunter"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 2.0, Vampire Hunters had Death Notes. This allowed them to be easily confirmed by revealing who they are in their Death Note. ",
                "If an Amnesiac becomes a Vampire Hunter, or a Retributionist revives them after all Vampires are dead, they will remain a Vampire Hunter for the rest of the game, unless another Amnesiac becomes a Vampire and dies.",
                "There was a bug where the Vampire Hunter was referred to as a unique role and was unable to be remembered by an Amnesiac or resurrected by a Retributionist. ",
                "While jailed, a Vampire Hunter could stake any Vampires who attempted to convert them. "
        )
    },
    VETERAN("veteran"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 2.0, Veterans had Death Notes. This allowed them to be easily confirmed by revealing who they are in their Death Note. ",
                "Prior to Version 2.1.0, Veterans would not lose automatically against a Werewolf in a 1v1 situation. "
        )
    },
    VIGILANTE("vigilante"){
        override val triviaEntries = mutableListOf(
                "The Vigilante is the only role that can appear in all Classic and Coven game modes, excluding the Lovers game mode in the Coven Expansion, which cannot have a Vigilante. ",
                "If the Vigilante is revived after shooting a Townie, they are able to shoot again if they are not out of bullets.",
                "After shooting a Townie, a Vigilante may appear to not commit suicide from guilt if they are executed by a Jailor or leave the game.",
                "If a Vigilante shoots a Townie using their last bullet, the Vigilante will still \"put their gun aside\" and commit suicide even though they have no more bullets. "
        )
    },
    BODYGUARD("bodyguard"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 2.3.2, Bodyguards were able to defend from and kill an Arsonist visiting their target. ",
                "There used to be a bug where the Werewolf would not be counter-attacked by a Bodyguard while jailed on a Full Moon. "
        )
    },
    DOCTOR("doctor"){
        override val triviaEntries = mutableListOf(
                "Along with the Lookout, the Doctor is one of the few unchanged roles."
        )
    },
    ESCORT("escort"){
        override val triviaEntries = mutableListOf(
                "The Escort's skin (Candy) has a very striking resemblance to the Disney character Jessica Rabbit.",
                "Prior to Version 2.0.0.6537, a Serial Killer would attack an Escort for roleblocking them even if they were jailed and/or executed. ",
                "If you are killed by the Serial Killer on the first day, there is only a ~52% chance that you have role blocked them."
        )
    },
    MAYOR("mayor"){
        override val triviaEntries = mutableListOf(
            "Mayor is one of four roles that have a day ability, the others being the Jailor, Pirate, and the Medium when dead. ",
            "As of Version 2.3.2.9706, a revealed mayor is unable to be healed if transported.",
            "As of Version 1.5.0.3431, a Revealed mayor can no longer whisper or be whispered to after Revealing."
        )
    },
    MEDIUM("medium"){
        override val triviaEntries = mutableListOf(
                "The \"Medium’s Curse\" is a popular joke that Mediums always get killed on the first night when their role is \"basically useless\", despite the player not having revealed their role",
                "The \"Hogan Scenario\" is when two Mediums are in the game and their miscommunication causes a snafu during the day.",
                "The Medium received a buff in Version 0.3.0 which was released on June 10th of 2014. Prior to then, the Medium would use its ability in the daytime to select one person from the graveyard to talk to anonymously during the night. In addition, once the Medium was dead, he did not have a seance available to communicate with the living. ",
                "According to a poll made by Naru2008, the Medium is the most hated role in Town of Salem which had 304 votes, saying \"you were just a sitting duck\", waiting to be killed. ",
                "The name Medium might have been derived from the French word \"médium\", which is a person who pretends to communicate with ghosts. "
        )
    },
    RETRIBUTIONIST("retributionist"){
        override val triviaEntries = mutableListOf(
                "If you attempt to resurrect a Townie who had just left the game, you will receive a message stating you are unable to. ",
                "If you are killed, you will still see the numbers and names of any dead Townies on the names list. ",
                "There was a bug where a Vampire Hunter could not be revived as the game stated it was a unique role, despite it not being a unique role. ",
                "There was a bug where cleaned targets could be revived even though they should not be. "
        )
    },
    TRANSPORTER("transporter"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 1.5.10, a Transporter could transport someone twice, giving them 2 transport messages and for sure confirming the existence of a Transporter without messing up vital investigative roles. ",
                "Prior to Version 2.0.0.6501, a Transporter could stop an Arsonist from incinerating their targets. ",
                "The Transporter is derived from SC2 Mafia's Bus Driver role. ",
                "The role of Transporter was once mistaken as a Town Protective due the typoon Transporter's role card. "
        )
    },
    DISGUISER("disguiser"){
        override val triviaEntries = mutableListOf(
                "Prior to Version 1.5.9, a Disguiser would trade bodies, names, and placement on the Village, but not Last Wills. This lead to the trend of people putting their names in their wills."
        )
    },
    FORGER("forger"){
        override val triviaEntries =  mutableListOf(
                "Since its introduction, the Forger is one of the few roles to have never been changed."
        )
    },
    FRAMER("framer"){
        override val triviaEntries = mutableListOf(
                "If an Investigator finds a \"Framer, Vampire, or Jester\" result during night one in a Ranked game, the chance of them really being a Framer is ~32%.",
                "According to a poll made by Naru2008, the Framer is the second most hated role in Town of Salem with 263 votes, for the reasoning that it was too slow and boring",
                "Because it is unlikely that the Framer visited the same person as an investigative role, a Sheriff will often not take the possibility of a Framer into account, should you frame someone who is investigated.",
                "The Framer joins the Forger, Lookout, and Doctor's as one of the few roles never ot have been changed."
        )
    },
    JANITOR("janitor"){
        override val triviaEntries = mutableListOf(
                "Once a target is cleaned, their role will be unknown to the Town without a Medium or Retributionist in the game to tell them.",
                "A cleaned target is unable to be revived."
        )
    },
    GODFATHER("godfather"){
        override val triviaEntries: List<String> = mutableListOf(
                "No Trivia Found."
        )
    },
    MAFIOSO("mafioso"){
        override val triviaEntries: List<String>
            get() = mutableListOf("No Trivia Found")
    }
}