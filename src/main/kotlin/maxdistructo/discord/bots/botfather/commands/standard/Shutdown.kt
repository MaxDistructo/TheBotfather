package maxdistructo.discord.bots.botfather.commands.standard

import maxdistructo.discord.bots.botfather.commands.Base
import maxdistructo.discord.core.jda_utils.DefaultCommand
import net.dv8tion.jda.api.entities.Message

class Shutdown : Base.BaseCommand(){
    override val requiresOwner: Boolean
        get() = true
    override val requiresAdmin: Boolean //To ensure even if registered in non Admin Listener, it can not be run by every person on the server.
        get() = true
    override val helpMessage: String
        get() = "@shutdown - Shuts the bot down."
    override val commandName: String
        get() = "shutdown"
    override val hasOutput: Boolean
        get() = false

    override fun init(message: Message, args: List<String>): String {
        return onShutdownCommand(message)
    }

    fun onShutdownCommand(message: Message): String {
            message.channel.sendMessage("Shutting Down").complete(true)
            System.exit(0)
            return ""
    }
}
