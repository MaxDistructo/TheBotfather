package maxdistructo.discord.bots.botfather.commands.mafia.methods

import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.commands.mafia.data.JSONStruct
import maxdistructo.discord.bots.botfather.commands.mafia.obj.Details
import maxdistructo.discord.bots.botfather.commands.mafia.data.Roles
import maxdistructo.discord.bots.botfather.commands.mafia.init.IRole
import maxdistructo.discord.core.Constants
import maxdistructo.discord.core.JSONUtils
import maxdistructo.discord.core.Utils
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.TextChannel
import net.dv8tion.jda.api.entities.User
import org.json.JSONException
import org.json.JSONObject

object MafiaConfig {

    private fun readDataFile(): JSONObject {
        return JSONUtils.readJSONFromFile(Constants.s + "/config/mafia.json")
    }
    private fun writeDataFile(data: JSONObject){
        JSONUtils.writeJSONToFile(Constants.s + "/config/mafia.json", data)
    }
    private fun getNewPlayerDat(long: Long): JSONStruct.UserData{
            return JSONStruct.UserData(
                    true,
                    0,
                    0,
                    false,
                    jailed = false,
                    role = Roles.ADMIN,
                    permLevel = 1,
                    wins = 0,
                    losses = 0,
                    revealed = false,
                    disguised = "",
                    cleaned = false,
                    id = long,
                    relay = ""
            )
    }

    fun addPlayer(long: Long){
        val player = getPlayer(long)
        if(player != null){
            editDetails(long, Details.TIER, 1)
        }
        else{
            val data = readDataFile()
            data.append("" + long, JSONStruct.userDataToJSON(getNewPlayerDat(long)))
            writeDataFile(data)
        }
    }

    fun getPlayer(member: Member): JSONStruct.UserData?{
        return getPlayer(member.idLong)
    }
    fun getPlayer(user: User): JSONStruct.UserData?{
        return getPlayer(user.idLong)
    }
    fun getPlayer(playerId: Long): JSONStruct.UserData?{
        val gameDat: JSONObject

        try {
             gameDat = readDataFile().getJSONObject("$playerId")
        }
        catch(e: JSONException){
            return null
        }

        return JSONStruct.UserData(
                gameDat.getBoolean("alive"),
                gameDat.getInt("attack"),
                gameDat.getInt("defence"),
                gameDat.getBoolean("framed"),
                gameDat.getBoolean("jailed"),
                Roles.valueOf(gameDat.getString("role")),
                gameDat.getInt("tier"),
                gameDat.getInt("wins"),
                gameDat.getInt("losses"),
                gameDat.getBoolean("revealed"),
                gameDat.getString("disguised"),
                gameDat.getBoolean("cleaned"),
                gameDat.getString("relay"),
                gameDat.getLong("id")
        )
    }
    fun getPlayers(): List<JSONStruct.UserData?> {
        return getPlayers(false)
    }
    fun getPlayers(aliveOnly: Boolean): List<JSONStruct.UserData?>{
        val gameDat = readDataFile()
        val outputList: MutableList<JSONStruct.UserData> = mutableListOf()
        for(player in gameDat.toMap()){
            if(Utils.convertToLong(player.key) != null) {
                val playerDat = getPlayer(Utils.convertToLong(player.key)!!)
                if(playerDat != null && playerDat.permLevel == 1){
                    if(aliveOnly && playerDat.alive) {
                        outputList.add(playerDat)
                    }
                    else{
                        outputList.add(playerDat)
                    }
                }
            }
        }
        return outputList
    }

    fun editDetails(long: Long, key : Details, value : Any){
        if(key == Details.ROLE){
            val role = value as IRole
            editDetails(long, key.cfgName, value)
            editDetails(long, Details.ATTACK, role.attack)
            editDetails(long, Details.DEFENCE, role.defence)
        }
        else {
            editDetails(long, key.cfgName, value)
        }
    }
    fun editDetails(long: Long, key: String, value: Any){
        val json = readDataFile()
        val userJson = json.getJSONObject("" + long)
        json.remove("" + long)
        userJson.remove(key)
        userJson.put(key, value)
        json.put("" + long, userJson)
        writeDataFile(json)
    }

    fun getJailed(): JSONStruct.UserData? {
        val players = getPlayers()
        for(player in players){
            if(player!!.jailed){
                return player
            }
        }
        return null
    }
    fun getGeneral(): TextChannel {
        val dataFile = readDataFile()
        return BaseBot.bot.client.getTextChannelById(dataFile.getLong("general_chat"))!!
    }
    fun getDayNumber(): Int{
        val dataFile = readDataFile()
        return dataFile.optInt("day")
    }
    fun incrementDay(){
        val dataFile = readDataFile()
        dataFile.put("day", getDayNumber() + 1)
        writeDataFile(dataFile)
    }
    fun checkDay(): Boolean{
        val dataFile = readDataFile()
        return dataFile.optBoolean("isDay")
    }
    fun toggleDay(){
        val dataFile = readDataFile()
        dataFile.put("isDay", !dataFile.optBoolean("isDay"))
        writeDataFile(dataFile)
    }
    fun getHostId(): Long{
        val dataFile = readDataFile()
        return dataFile.optLong("host_id")
    }
    fun setHostId(hostId: Long){
        val dataFile = readDataFile()
        dataFile.put("host_id", hostId)
        writeDataFile(dataFile)
    }
    fun getExecute(): Boolean{
        val dataFile = readDataFile()
        return dataFile.optBoolean("execute")
    }
    fun toggleExecute() {
        val dataFile = readDataFile()
        val status = getExecute()
        dataFile.remove("execute")
        dataFile.put("execute", !status) //Quick toggle using the inverse statement of !
        writeDataFile(dataFile)
    }
}