package maxdistructo.discord.bots.botfather.commands.mafia.methods

import maxdistructo.discord.bots.botfather.commands.mafia.Perms
import maxdistructo.discord.bots.botfather.commands.mafia.obj.Details
import maxdistructo.discord.core.JSONUtils
import net.dv8tion.jda.api.entities.Message

object Toggle {
    fun fixAlivePlayerPermsDay(message : Message){
        val dayChannel = MafiaConfig.getGeneral()
        for(player in MafiaConfig.getPlayers()) {
            if (player!!.permLevel >= 2) {
                try {
                    dayChannel.getPermissionOverride(message.guild.getMemberById(player.id)!!)!!.delete().complete(true)
                }
                catch(e : NullPointerException){}
                if(player.alive) {
                    Perms.allowReadWrite(dayChannel, player.id)
                }
                else{
                    Perms.allowRead(dayChannel, player.id)
                }
                //Disable All Relays
                MafiaConfig.editDetails(player.id, Details.RELAY, "")
            }
        }
    }
    fun fixAlivePlayerPermsNight(message : Message) {
        for(player in MafiaConfig.getPlayers()) {
            if (player!!.permLevel >= 2) {
                Perms.removePerms(MafiaConfig.getGeneral(), player!!.id)
                println("Ran Night Toggle")
            }
        }
    }
    fun fixDeadPerms(message : Message){
        for(player in MafiaConfig.getPlayers()) {
            if (!player!!.alive) {
                Perms.removePerms(MafiaConfig.getGeneral(), player.id)
            }
        }
    }
    fun updateTopic(message : Message){
        val alivePlayers = mutableListOf<String>()
        val deadPlayers = mutableListOf<String>()
        //Todo Revisit This
        //game.dayChannel.manager.setTopic("Day $daynumber - $alivePlayers alive, $deadPlayers dead").complete(true)
    }
}