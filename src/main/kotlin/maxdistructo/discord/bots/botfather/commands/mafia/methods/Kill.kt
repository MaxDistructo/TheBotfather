package maxdistructo.discord.bots.botfather.commands.mafia.methods

import maxdistructo.discord.bots.botfather.commands.mafia.data.JSONStruct
import maxdistructo.discord.bots.botfather.commands.mafia.data.Roles
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message


object Kill {
    fun message(message1: Message, messageContent: Array<Any>): String {
        val message = StringBuilder() // 0 = Command, 1 = Subcommand, 2 = User 3 = Param 1
        var mentioned: Member? = null
        val player: JSONStruct.UserData
        try {
            mentioned = JDAUtils.getUserFromInput(message1, messageContent[2])!!
        } catch (e: Exception) {
            Messages.throwError(e)
        }
        player = MafiaConfig.getPlayer(mentioned!!)!!
        message.append(mentioned.asMention)
        /*
        when {
            messageContent[3] == "-2kill" -> {
                message.append(single.getString(messageContent[4].toString()))
                message.append(" ")
                message.append(multi.getString(messageContent[5].toString()))
            }
            messageContent[3] == "-3kill" -> {
                message.append(single.getString(messageContent[4].toString()))
                message.append(" ")
                message.append(multi.getString(messageContent[5].toString()))
                message.append(" ")
                message.append(multi.getString(messageContent[6].toString()))
            }
            else ->
                message.append(single.getString(messageContent[3].toString()))
        }
         */
        if (!player.cleaned) {
            message.append(" ")

            if(player.role == Roles.DISGUISER) {
                message.append("Their role was __**" + player.disguised + "**__.")
            }
            else{
                message.append("Their role was __**" + player.role.regName + "**__.")
            }
        } else {
            message.append(" ")
            message.append("Their role was Cleaned")
        }

        return message.toString()
    }
}