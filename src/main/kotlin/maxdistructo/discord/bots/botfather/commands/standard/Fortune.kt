package maxdistructo.discord.bots.botfather.commands.standard

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import com.mashape.unirest.http.Unirest
import maxdistructo.discord.bots.botfather.commands.Base
import maxdistructo.discord.core.jda.message.Messages
import org.json.JSONObject
import net.dv8tion.jda.api.entities.Message

class Fortune : Command() {

    init{
        this.name = "fortune"
        this.help = "fortune - Gets your fortune"
    }

    private fun onFortuneCommand(message: Message): String {
        var fortune: JSONObject? = null
        try {
            fortune = Unirest.get("https://helloacm.com/api/fortune").asJson().body.`object`
        } catch (e: Exception) {
            Messages.throwError(e, message)
        }
        println(fortune)

        return if (fortune != null) {
            "Your fortune is " + fortune.get("fortune")
        } else "Your fortune seems to be out of reach. (API Error)"
    }

    override fun execute(event: CommandEvent) {
        event.reply(Messages.simpleEmbed(event.member, onFortuneCommand(event.message),event.message.guild))
    }
}
