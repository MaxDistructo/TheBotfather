package maxdistructo.discord.bots.botfather.commands.mafia.methods

import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.commands.mafia.data.Classification
import maxdistructo.discord.bots.botfather.commands.mafia.data.JSONStruct
import maxdistructo.discord.bots.botfather.commands.mafia.data.Roles
import maxdistructo.discord.bots.botfather.commands.mafia.generator.RoleGenerator
import maxdistructo.discord.bots.botfather.commands.mafia.init.IRole
import maxdistructo.discord.bots.botfather.commands.mafia.obj.Details

import maxdistructo.discord.core.JSONUtils
import maxdistructo.discord.core.UnicodeEmoji
//import maxdistructo.discord.core.jda.Roles
import maxdistructo.discord.core.Utils
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages

import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.User
import org.json.JSONObject

object Mafia {

    fun onGameJoinCommand(message: Message) {
        MafiaConfig.addPlayer(message.author.idLong)
        Messages.sendMessage(message.channel, message.author.asMention + ", you have been added to the Mafia game.")
        message.addReaction(UnicodeEmoji.HEAVY_CHECK_MARK).queue()
    }

    fun onGameLeaveCommand(message: Message) {
        MafiaConfig.editDetails(message.author.idLong, Details.TIER, 0)
        Messages.sendMessage(message.channel, message.member!!.asMention + ", you have been removed from the game.")
        message.addReaction(UnicodeEmoji.HEAVY_CHECK_MARK).queue()
    }

    fun broadcast(message: String) {
        val players = MafiaConfig.getPlayers()
        for(player in players){
            Messages.sendDM(BaseBot.client.getUserById(player!!.id)!!, message)
        }
    }

    fun broadcastDead(message: String){
        val players = MafiaConfig.getPlayers()
        for(player in players){
            if(!player!!.alive) {
                Messages.sendDM(BaseBot.client.getUserById(player!!.id)!!, message)
            }
        }
    }

    fun onGameStart(message: Message) {
        val messageArgs = message.contentRaw.split(" ")
        var error = false

        println("Starting Game")
        if(messageArgs[2].contains(',')) {
            println("Generating roles for players")
            assignRoles(messageArgs[2])
        }
        else{
            if(messageArgs[2] == "classic"){
                assignRoles("sheriff,doctor,investigator,jailor,medium,godfather,framer,executioner,escort,"
                        + "mafioso,lookout,serial_killer,tk,jester,rt")
            }
            else if(messageArgs[2] == "rp" || messageArgs[2] == "ranked_practice"){
                assignRoles("jailor,ti,ti,tp,tk,ts,rt,rt,rt,godfather,mafioso,rm,rm,ne,nk")
            }
            else if(messageArgs[2] == "rainbow"){
                assignRoles("godfather,arsonist,survivor,jailor,amnesiac,serial_killer,witch,any,witch,"
                        + "serial_killer,amnesiac,veteran,survivor,arsonist,mafioso")
            }
            else if(messageArgs[2] == "dracula" || messageArgs[2] == "dracula's_palace" || messageArgs[2] == "dracula's"){
                assignRoles("doctor,lookout,lookout,jailor,vigilante,tp,ts,ts,vampire_hunter,jester,witch,"
                        + "vampire,vampire,vampire,vampire")
            }
            else if(messageArgs[2] == "vigilantics"){
                assignRoles("vigilante,witch,vigilante,vigilante,witch,vigilante,vigilante,witch,"
                        + "vigilante,vigilante,vigilante,witch,vigilante,vigilante,witch")
            }
            else if(messageArgs[2] == "allany" || messageArgs[2] == "any" || messageArgs[2] == "aa"){
                assignRoles("any,any,any,any,any,any,any,any,any,any,any,any,any,any,any")
            }
            else{
                Messages.sendMessage(message.channel, "Invalid Rolelist Used. Please use a custom definition or one of the presets.")
                error = true
            }
        }

        if(!error) {
            for (player in MafiaConfig.getPlayers()) {
                Messages.sendDM(BaseBot.client.getUserById(player!!.id)!!, RoleCards.generate(player.role),
                        false)
            }

            Toggle.fixAlivePlayerPermsDay(message)
            Toggle.updateTopic(message)
        }
    }

    fun onGameToggle(message: Message) {
        if(MafiaConfig.checkDay()){
            toggleToNight(message)
        }
        else{
            toggleToDay(message)
        }
    }

    private fun toggleToDay(message : Message){
        val players = MafiaConfig.getPlayers(true)
        val daynumber = MafiaConfig.getDayNumber() + 1
        val votesIn = players.size
        var votes = votesIn / 2

        MafiaConfig.incrementDay()
        if(!MafiaConfig.checkDay()){
            MafiaConfig.toggleDay()
        }
        Messages.sendDM(BaseBot.client.getUserById(MafiaConfig.getHostId())!!, "Starting change over to day mode. Please wait.")
        //runActions(message)
        //resetChannelOverrides(message, players)
        Toggle.fixAlivePlayerPermsDay(message)
        Toggle.fixDeadPerms(message)

        if(votesIn % 2 > 0){
            votes++
        }
        Toggle.updateTopic(message)
        Messages.sendMessage(MafiaConfig.getGeneral(), "Day $daynumber has started. $votes votes needed to vote someone up.")
    }
    private fun toggleToNight(message : Message){
        val players = MafiaConfig.getPlayers()
        val dayNum = MafiaConfig.getDayNumber()
        if(MafiaConfig.checkDay()){
            MafiaConfig.toggleDay()
        }
        //resetChannelOverrides(message, players)
        Toggle.fixAlivePlayerPermsNight(message)
        Toggle.updateTopic(message)
        //Messages.sendMessage(game.dayChannel, " It is now Night $dayNum")
    }

    fun assignRoles(rolelist: String) {
        val roleList = RoleGenerator.generateFromList(rolelist.split(","))
        for(value in roleList){
            val user = BaseBot.client.getUserById(value.first.id)!!
            MafiaConfig.editDetails(value.first.id, Details.ROLE, value.second)
            MafiaConfig.editDetails(value.first.id, Details.ATTACK, value.second.attack)
            MafiaConfig.editDetails(value.first.id, Details.DEFENCE, value.second.defence)
            Messages.sendDM(user, "A new game has begun! Your role is: ")
            Messages.sendDM(user, RoleCards.generate(value.second))
        }
    }

    @Deprecated("Use MafiaConfig for all changes to players.")
    fun killPlayer(message: Message, playerID: Long): String {
        val player = MafiaConfig.getPlayer(playerID)!!
        val member = message.guild.getMemberById(player.id)!!
        if (!player.alive) {
            return "Can not kill player " + member.effectiveName + " as they are already dead."
        }
        MafiaConfig.editDetails(player.id, Details.ALIVE, false)
        return "Successfully killed player " + member.effectiveName
    }

    fun jailPlayer(message: Message, user: Member) {
        val jailor = MafiaConfig.getPlayer(message.author.idLong)
        if(jailor!!.role != Roles.JAILOR || jailor.role == Roles.ADMIN){
            message.channel.sendMessage("You attempted to pull " + user.asMention + "to Jail but you do not " +
                    " have the keys to the cell!")
        }
        else {
            if(message.author != user.user) {
                MafiaConfig.editDetails(user.idLong, Details.JAILED, true)
                Messages.sendDM(message.author, "You have sucessfully jailed your target.")
            }
            else{
                Messages.sendDM(message.author, "You cannot jail yourself!")
            }
        }
    }

    fun unjail() {
        MafiaConfig.editDetails(MafiaConfig.getJailed()!!.id, Details.JAILED, false)
    }

    fun setRole(message: Message, user: Member, role: String) {
        MafiaConfig.editDetails(user.idLong, Details.ROLE, RoleGenerator.getRole(role))
    }

    fun getUserFromInput(message : Message, input : Any, mentionNum : Int) : Member?{
        return getUserFromInput(message, input, mentionNum, 0)
    }

    fun getUserFromInput(message : Message, input : Any) : Member?{
        return getUserFromInput(message, input, 0)
    }

    private fun getUserFromInput(message : Message, input : Any, mentionNum : Int, slotIn : Int) : Member? {
        //Code is identical to maxdistructo.discord.core.Utils.getUserFromInput
        //Variant is because users are being returned that are not a part of the mafia game.
        val attemptedUser: Member? = when {
            JDAUtils.getMentionedUser(message) != null -> JDAUtils.getMentionedUser(message)!!
            Utils.convertToLong(input) != null -> message.guild.getMemberById(Utils.convertToLong(input)!!)
            message.guild.getMembersByName (input.toString(), true).isNotEmpty() -> message.guild.getMembersByName(input.toString(), true)[slotIn]
            message.guild.getMembersByNickname(input.toString(), true).isNotEmpty() -> message.guild.getMembersByNickname(input.toString(), true)[slotIn]
            else -> userForLoop(message, input, slotIn)
        } ?: return null
        return attemptedUser
        /*
        if (MafiaConfig.getPlayers().contains(attemptedUser!!.user.idLong)) {
            return attemptedUser
        } else if (!MafiaConfig.getPlayers().contains(attemptedUser.user.idLong)) {
            return getUserFromInput(message, input, mentionNum, (slotIn + 1))
        }
        return null

         */
    }

    private fun userForLoop(message : Message, input : Any, slot : Int) : Member?{
        var i = 0
        println("Checking slot $slot")
        for(user in message.guild.members) {
            if (user.effectiveName.contains(input.toString())) {
                if(i == slot){
                    return user
                }
                else{
                    i++
                }
            }
            else if(user.user.name.contains(input.toString())){
                if(i == slot){
                    return user
                }
                else{
                    i++
                }
            }
        }
        return null
    }

    fun revive(message : Message, user : Long){
        MafiaConfig.editDetails(user, Details.ALIVE, true)
    }

    fun permFixer(message : Message){
        /*
        val role = Roles.getRole(message, "Alive(Mafia)")!!
        val deadRole = Roles.getRole(message, "Dead(Mafia)")!!
        for(player in MafiaConfig.getPlayers()){
            val player2 = Player(message, player)
            if(!player2.dead) {
                message.guild.addRoleToMember(message.guild.getMemberById(player)!!, role).submit() //For all players, give the alive role.
                try { //If player has dead role, remove it because they are actually alive
                    message.guild.removeRoleFromMember(message.guild.getMemberById(player)!!, deadRole).submit()
                }
                catch(e : Exception){}
            }
            else{
                message.guild.addRoleToMember(message.guild.getMemberById(player)!!, deadRole).submit()
                try { //If player has alive role, remove it because they are actually dead
                    message.guild.removeRoleFromMember(message.guild.getMemberById(player)!!, role).submit()
                }
                catch(e : Exception){}
            }
        }
        resetChannelOverrides(message, MafiaConfig.getPlayers())
        if(game.day){
            Toggle.fixAlivePlayerPermsDay(message, game)
            Toggle.fixDeadPerms(message, game)
        }
        else{
            Toggle.fixAlivePlayerPermsNight(message, game)
            Toggle.fixDeadPerms(message, game)
        }
         */
    }
}
