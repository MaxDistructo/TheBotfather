package maxdistructo.discord.bots.botfather.commands

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import maxdistructo.discord.core.Utils
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.TextChannel


object Base {

    @Deprecated("Use JDAUtils Command API") // TODO(REMOVE THIS AND CONVERT ALL USES) ASAP
    abstract class BaseCommand() : Command() {

        final override fun execute(event: CommandEvent) { //Allows for my command style to work with the JDA-Utils version but not require commands to be rewritten
            if(!this.hasOutput) {
                event.reply(this.init(event.message, event.message.contentDisplay.split(" ")))
            }
            else{
                this.init(event.message, event.message.contentDisplay.split(" "))
            }
        }



        open val isEventCommand: Boolean
            get() = false

        private var type = ICommandType.NORMAL
        open val commandType: Enum<ICommandType> = type
        open val commandName : String = name
        open val requiresAdmin: Boolean
            get() = false
        open val helpMessage: String
            get() = "command <params> - A description of the command"
        open val requiresMod: Boolean
            get() = false
        open val requiresGuildOwner: Boolean
            get() = false
        open val requiresOwner: Boolean
            get() = false
        open val hasOutput : Boolean //Has Output to be outputted by the listener
            get() = true
        open val isSubcommand: Pair<Boolean, String>
            get() = Pair(false, "")

        init{
            this.guildOnly = true
            this.name = this.commandName
            this.help = this.helpMessage
        }

        constructor(commandNameIn: String, typeIn : Enum<ICommandType>) : this() {
            name = commandNameIn
            type = typeIn as ICommandType
            this.help = this.helpMessage
        }

        open fun init(message : Message, args : List<String>) : String {
            return ""
        }

    }
    enum class ICommandType{
        NORMAL,
        ADMIN,
        GAME
    }

}