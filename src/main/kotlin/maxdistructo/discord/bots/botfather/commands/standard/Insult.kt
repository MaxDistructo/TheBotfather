package maxdistructo.discord.bots.botfather.commands.standard

import com.jagrosh.jdautilities.command.Command
import com.jagrosh.jdautilities.command.CommandEvent
import com.mashape.unirest.http.Unirest
import com.mashape.unirest.http.exceptions.UnirestException
import maxdistructo.discord.bots.botfather.BaseBot
import maxdistructo.discord.bots.botfather.background.PrivUtils
import maxdistructo.discord.bots.botfather.commands.Base
import maxdistructo.discord.core.Utils
import maxdistructo.discord.core.jda.JDAUtils
import maxdistructo.discord.core.jda.message.Messages
import org.json.JSONObject
import net.dv8tion.jda.api.entities.Message
import net.dv8tion.jda.api.entities.Member
import net.dv8tion.jda.api.entities.User
import java.util.concurrent.ThreadLocalRandom

class Insult : Command() {
    init{
        this.name = "insult"
        this.help = "insult <@User> - Insults the provided user."
    }

    private fun onInsultCommand(message: Message, mentioned: Member): String {
        val author = message.member!!
        val name = mentioned.effectiveName
        val from = author.effectiveName
        val insults = arrayOf("anyway/$name/$from", "asshole/$from", "bag/$from", "ballmer/$name/$name/$from", "blackadder/$name/$from")
        val randomNum = ThreadLocalRandom.current().nextInt(0, insults.size)
        if (message.jda.selfUser as User === mentioned.user) {
            return "How original. No one *cough* ${BaseBot.client.getUserById(BaseBot.bot.commandAPI.ownerId)!!.asMention} *cough* else had thought of trying to get the bot to insult itself. I applaud your creativity. \"yawn\" Perhaps this is why you don't have friends. You don't add anything new to any conversation. You are more of a bot than me, predictable answers, and absolutely dull to have an actual conversation with."
        } else {
            var insult: JSONObject? = null
            try {
                insult = Unirest.get("http://foaas.herokuapp.com/" + insults[randomNum]).header("Accept", "application/json").asJson().body.`object`
            } catch (e: UnirestException) {
                e.printStackTrace()
            }
            //println(insult)
            return if (insult != null) {
                insult.getString("message")
            } else {
                "This bot is so stupid, your stupidity is even worse. (API Error)"
            }
        }

    }

    override fun execute(event: CommandEvent) {
        val message = event.message
        event.reply(Messages.simpleEmbed(event.member, onInsultCommand(message, JDAUtils.getMentionedUser(message)!!), message))
    }
}
