package maxdistructo.discord.bots.botfather.commands.mafia.data

import maxdistructo.discord.bots.botfather.commands.mafia.init.IAlignment
import maxdistructo.discord.bots.botfather.commands.mafia.init.IRole
import java.awt.Color
import java.net.URL

enum class Roles(override val regName : String): IRole {
    INVEST("investigator"){
        override val classification = Classification.TI
        override val alignment: IAlignment = Alignments.TOWN
        override val summary = "A private eye who secretly gathers information."
        override val description= "Investigate one person each night for a clue to their role."
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/c/cb/Investigator.png/revision/latest?cb=20141222203926"
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/2/2d/Investmobile.png/revision/latest/scale-to-width-down/48?cb=20190110070357"
        override val attack = 0
        override val defence = 0
        override val isUnique = false
        override fun getColor(): Color {
            return alignment.color
        }
    },
    LOOKOUT("lookout") {
        override val classification = Classification.TI
        override val alignment: IAlignment = Alignments.TOWN
        override val summary = "An eagle-eyed observer, stealthily camping outside houses to gain information."
        override val description = "Watch one person at night to see who visits them."
        override val thumbnail = "" //Does not exist
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/9/9d/Lomobile.png/revision/latest/scale-to-width-down/50?cb=20190110070744"
        override val attack = 0
        override val defence = 0
        override val isUnique = false
        override fun getColor(): Color {
            return alignment.color
        }
    },
    SHERIFF("sheriff"){
        override val classification = Classification.TI
        override val alignment: IAlignment = Alignments.TOWN
        override val summary = "The law enforcer of the Town, forced into hiding from threat of murder."
        override val description = "Interrogate one person each night for suspicious activity."
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/3/3e/Sheriff.png/revision/latest/scale-to-width-down/150?cb=20140802032529" //Does not exist
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/2/25/Shermobile.png/revision/latest?cb=20190110072314"
        override val attack = 0
        override val defence = 0
        override val isUnique = false
        override fun getColor(): Color {
            return alignment.color
        }
    },
    SPY("spy"){
        override val classification = Classification.TI
        override val alignment: IAlignment = Alignments.TOWN
        override val summary = "A talented watcher who keeps track of evil in the Town."
        override val description = "You may bug a player's house to see what happens to them that night."
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/3/36/Spy.png/revision/latest/scale-to-width-down/150?cb=20151129195522" //Does not exist
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f4/Spymobile.png/revision/latest/scale-to-width-down/50?cb=20190110072412"
        override val attack = 0
        override val defence = 0
        override val isUnique = false
        override fun getColor(): Color {
            return alignment.color
        }
    },
    ESCORT("escort"){
        override val alignment = Alignments.TOWN
        override val attack = 0
        override val defence = 0
        override val summary = "A beautiful woman skilled in distraction."
        override val description = "Distract someone each night. Distraction blocks your target from using their role's night ability. You cannot be role blocked."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/c/c8/Escmobile.png/revision/latest/scale-to-width-down/50?cb=20190110065545"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/d/d3/Escort.png/revision/latest?cb=20150506224645"
        override val classification = Classification.TS
        override fun getColor(): Color {
            return alignment.color
        }

    },
    MAYOR("mayor"){
        override val alignment = Alignments.TOWN
        override val attack = 0
        override val defence = 0
        override val summary = "The leader of the Town."
        override val description = "You may reveal yourself as the Mayor of the Town. Once you have revealed yourself as Mayor your vote counts as 3 votes. You may not be healed once you have revealed yourself."
        override val isUnique: Boolean = true
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f3/Mayormobile.png/revision/latest/scale-to-width-down/50?cb=20190110071008"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/8/8b/MoneyBag.png/revision/latest?cb=20141029221203"
        override val classification: String = Classification.TS
        override fun getColor(): Color {
            return alignment.color
        }
    },
    MEDIUM("medium"){
        override val classification = Classification.TS
        override val alignment = Alignments.TOWN
        override val attack = 0
        override val defence = 0
        override val summary = "A secret Psychic who talks with the dead."
        override val description = "You can speak to the dead and they can speak to you. All messages you send to the dead are anonymous. Once you die you are able to tell the town one message."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e9/Medmobile.png/revision/latest?cb=20190110071103"
        override val thumbnail = "https://i.imgur.com/WBTx4Kx.png"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    RETRIBUTIONIST("retributionist"){
        override val classification = Classification.TS
        override val isUnique: Boolean = true
        override val alignment = Alignments.TOWN
        override val attack = 0
        override val defence = 0
        override val summary = "A powerful mystic who will give one person a second chance at life."
        override val description = "You may revive 1 dead Town member."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/3/33/Retmobile.png/revision/latest/scale-to-width-down/50?cb=20190110072041"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/Nightspirit174%27s_Retributionist_Avatar.png/revision/latest?cb=20160423042204"
        override fun getColor(): Color {
            return alignment.color
        }
    }, //Short for Retributionist
    TRANSPORTER("transporter"){
        override val classification = Classification.TS
        override val isUnique: Boolean = false
        override val alignment = Alignments.TOWN
        override val attack = 0
        override val defence = 0
        override val summary = "A man who transports people without asking any questions."
        override val description = "Choose two people to transport at night. Transporting two people swaps all targets against them. You may transport yourself. Your targets will know they were transported. You may not transport someone with themselves. You can not transport Jailed people."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f6/Transmobile.png/revision/latest/scale-to-width-down/50?cb=20190110072616"
        override val thumbnail = ""
        override fun getColor(): Color {
            return alignment.color
        }

    }, //Short for Transporter
    BODYGUARD("bodyguard"){
        override val classification = Classification.TP
        override val isUnique: Boolean = false
        override val alignment = Alignments.TOWN
        override val attack = 2
        override val defence = 0
        override val summary = "An ex-soldier who secretly makes a living by selling protection."
        override val description = "Protect a player from direct attacks at night. If your target is attacked or is the victim of a harmful visit you and the visitor will fight. If you successfully protect someone you can still be Healed."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/3/38/Bgmobile.png/revision/latest?cb=20190110064542"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/8/87/Bodyguard.png/revision/latest?cb=20150207170237"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    DOCTOR("doctor"){
        override val alignment = Alignments.TOWN
        override val attack = 0
        override val defence = 0
        override val summary = "A surgeon skilled in trauma care who secretly heals people."
        override val description = "Heal one person each night preventing them from dying. You may only Heal yourself once. You will know if your target is attacked."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/a/ab/Docmobile.png/revision/latest/scale-to-width-down/50?cb=20190110065435"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/6/6c/Doctor.png/revision/latest?cb=20140802032512"
        override val classification: String = Classification.TP
        override fun getColor(): Color {
            return alignment.color
        }
    },
    VAMP_HUNTER("vampire_hunter"){
        override val classification = Classification.TK
        override val isUnique: Boolean = false
        override val alignment = Alignments.TOWN
        override val attack = 1
        override val defence = 0
        override val summary = "A priest turned monster hunter"
        override val description = "Check for Vampires each night. If you find a Vampire you will attack them. If a Vampire visits you you will attack them. You can hear Vampires at night. If all the Vampires die you will become a Vigilante."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/7/7e/Vhmobile.png/revision/latest/scale-to-width-down/50?cb=20190110072912"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/c/c8/Vampire_Hunter.png/revision/latest/scale-to-width-down/150?cb=20151101133904"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    VETERAN("veteran"){
        override val alignment = Alignments.TOWN
        override val isUnique: Boolean = true
        override val attack = 2
        override val defence = 0
        override val summary = "A paranoid war hero who will shoot anyone who visits him."
        override val description = "Decide if you will go on alert tonight. If you go on alert anyone who visits will die."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/7/71/Vetmobile.png/revision/latest/scale-to-width-down/50?cb=20190110073013"
        override val thumbnail = ""
        override val classification = Classification.TK
        override fun getColor(): Color {
            return alignment.color
        }
    },
    VIGILANTE("vigilante"){
        override val classification = Classification.TK
        override val isUnique: Boolean = false
        override val alignment = Alignments.TOWN
        override val attack = 1
        override val defence = 0
        override val summary = "A militant officer willing to bend the law to enact justice."
        override val description = "Choose to take justice into your own hands and shoot someone. You only have 3 shots and if you kill another town member you will suicide."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/8/8e/Vigimobile.png/revision/latest?cb=20190110073114"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/8/8e/SoS_Vigilante_Skin.png/revision/latest/scale-to-width-down/122?cb=20200319034407"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    DISGUISER("disguiser"){
        override val classification = Classification.MD
        override val isUnique: Boolean = false
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val summary = "A master of disguise who pretends to be other roles."
        override val description = "Choose a target to disguise yourself as. You will appear to be the role of your target to the Investigator. If you are killed you will appear to be the role of your target."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/9/9a/Disgmobile.png/revision/latest?cb=20190110065321"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e4/NasubiNori_Disguiser_Avatar.png/revision/latest?cb=20180311074829"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    FRAMER("framer"){
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val summary = "A skilled counterfeiter who manipulates information."
        override val description = "Choose someone to frame at night. If your target is investigated they will appear to be a member of the Mafia."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/d/df/Framermobile.png/revision/latest?cb=20190110065903"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/ForgerAvatar.png/revision/latest?cb=20150724030632"
        override val classification: String = Classification.MD
        override fun getColor(): Color {
            return alignment.color
        }
    },
    FORGER("forger"){
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val summary = "A crooked lawyer that replaces documents."
        override val description = "If you chose to use your ability the next person the mafia kills will get an improper response. You only have 3 uses of this role."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/a/a2/Forgmobile.png/revision/latest?cb=20190110065758"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/6/66/ForgerAvatar.png/revision/latest?cb=20150724030632"
        override val classification = Classification.MD
        override fun getColor(): Color {
            return alignment.color
        }
    },
    JANITOR("janitor"){
        override val classification = Classification.MD
        override val isUnique: Boolean = false
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val summary = "A sanitation expert working for organized crime."
        override val description = "Choose a person to clean at night. If they die their role will not be revealed to the town. You will know their role though. You only have 3 uses of this ability."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/7/78/Janmobile.png/revision/latest?cb=20190110070644"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/4/4d/Yuan_Itor.png/revision/latest/scale-to-width-down/154?cb=20160826042531"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    BLACKMAILER("blackmailer"){
        override val classification = Classification.MS
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val summary = "An eavesdropper who uses information to keep people quiet."
        override val description = "Choose one person each night to blackmail. Blackmailed targets can not talk during the day. You can hear private messages."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f9/Bmermobile.png/revision/latest/scale-to-width-down/50?cb=20190110064403"
        override val thumbnail = "https://orig00.deviantart.net/3959/f/2016/043/f/0/blackmailer_skin_5_by_purplellamas_town_of_salem_by_purplellamas5-d9ri8cf.png"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    CONSIG("consigliere"){
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val isUnique: Boolean = false
        override val summary: String = "A corrupted Investigator who gathers information for the Mafia."
        override val description : String = "Check one person for their exact role each night."
        override val thumbnail: String = ""
        override val icon: String = "https://vignette.wikia.nocookie.net/town-of-salem/images/a/a8/Consigmobile.png/revision/latest/scale-to-width-down/50?cb=20190110064717"
        override val classification: String = Classification.MS
        override fun getColor(): Color {
            return alignment.color
        }

    }, //Short for Consigliere
    CONSORT("consort"){
        override val classification = Classification.MS
        override val isUnique: Boolean = false
        override val alignment = Alignments.MAFIA
        override val attack = 0
        override val defence = 0
        override val summary = "A beautiful dancer working for organized crime."
        override val description = "Distract someone each night. Distraction blocks your target from using their role's night ability."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/c/c2/Consmobile.png/revision/latest?cb=20190110064907"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/1/15/Consort_Skin.png/revision/latest/scale-to-width-down/110?cb=20200319031450"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    GODFATHER("godfather"){
        override val classification = Classification.MK
        override val alignment = Alignments.MAFIA
        override val attack = 1
        override val defence = 1
        override val summary = "The leader of organized crime."
        override val description = "You decide who the mafia will kill. If you are role blocked the Mafioso's kill will be used instead of your own. You are unable to be found by a Sheriff."
        override val isUnique: Boolean = true
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/6/67/Gfmobile.png/revision/latest?cb=20190110063957"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/9/97/Godfather_2.png/revision/latest/scale-to-width-down/100?cb=20160606042812"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    MAFIOSO("mafioso"){
        override val classification = Classification.MK
        override val isUnique: Boolean = true
        override val alignment = Alignments.MAFIA
        override val attack = 1
        override val defence = 0
        override val summary = "A member of organized crime"
        override val description = "You may choose a target to attack. If the Godfather chooses his own then you will attack the Godfather's target instead. If the Godfather dies you will be promoted to Godfather."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/f/f4/Mafmobile.png/revision/latest?cb=20190110070843"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e3/Mafioso_Skin.png/revision/latest/scale-to-width-down/100?cb=20181221193552"
        override fun getColor(): Color {
            return alignment.color
        }
    },
    SERIAL_KILLER("serial_killer"){
        override val isUnique: Boolean = false
        override val alignment = Alignments.NEUTRAL_KILLING
        override val attack = 1
        override val defence = 1
        override val summary = "A psychotic criminal who wants everyone to die."
        override val description = "You may kill a target every night. If someone tries to roleblock you they will die. If you are Jailed and not executed you will kill the Jailor."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/3/3c/Skmobile.png/revision/latest/scale-to-width-down/50?cb=20190110072143"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/7/75/SerialKiller.png/revision/latest?cb=20140816021322"
        override val classification = Classification.NK
        override fun getColor(): Color {
            return Color(0x0A0984)
        }
    },
    WEREWOLF("werewolf"){
        override val classification = Classification.NK
        override val alignment = Alignments.NEUTRAL_KILLING
        override val attack = 2
        override val defence = 1
        override val summary = "A normal citizen who transforms during the full moon."
        override val description = "You will go on a rampage every other night. Going on a rampage means that all people who visit your target will die. If you do not chose a target then all people who visit you will die"
        override val isUnique: Boolean = true
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/f/fd/Wwmobile.png/revision/latest/scale-to-width-down/50?cb=20190110073211"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/c/c1/Lycanthrope.png/revision/latest/scale-to-width-down/151?cb=20160506214350"
        override fun getColor(): Color {
            return Color(0x825037)
        }
    },
    ARSONIST("arsonist"){
        override val classification = Classification.NK
        override val isUnique: Boolean = false
        override val alignment = Alignments.NEUTRAL_KILLING
        override val attack = 3
        override val defence = 1
        override val summary = "A pyromaniac that wants to burn everyone."
        override val description = "You may Douse someone in gasoline or ignite Doused targets. If you are doused and do no action one night you will clean off the gas from another Arsonist."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/b/b7/Arsomobile.png/revision/latest?cb=20190110064227"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/c/c3/Arsonist.png/revision/latest?cb=20141029221117"
        override fun getColor(): Color {
            return Color(0xEB8211)
        }
    },
    EXECUTIONER("executioner"){
        override val alignment = Alignments.NEUTRAL_EVIL
        override val attack = 0
        override val defence = 1
        override val summary = "An obsessed lyncher who will stop at nothing to execute his target."
        override val description = "Trick the Town into lynching your target. If your target gets killed by anyone else you will become a Jester."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/3/38/Exemobile.png/revision/latest/scale-to-width-down/50?cb=20190110065651"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/4/48/Executioner_Avatar.png/revision/latest?cb=20181007055854"
        override val classification: String = Classification.NE
        override fun getColor(): Color {
            return Color(0xCCCCCC)
        }
    },
    JESTER("jester"){
        override val classification = Classification.NE
        override val alignment = Alignments.NEUTRAL_EVIL
        override val attack = 3
        override val defence = 0
        override val summary = "A crazed lunatic whose life goal is to be publicly executed."
        override val description = "Trick the town into lynching you."
        override val isUnique: Boolean = false
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e6/Jestmobile.png/revision/latest?cb=20190110063406"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e0/Jester.png/revision/latest?cb=20140716035330"
        override fun getColor(): Color {
            return Color.PINK //TODO: Change me to actual color later
        }
    },
    WITCH("witch"){
        override val classification = Classification.NE
        override val isUnique: Boolean = false
        override val alignment = Alignments.WITCH_NE
        override val attack = 0
        override val defence = 0
        override val summary = "A voodoo master who can control other people's actions."
        override val description = "Control someone each night. You can force people to target themselves. Your victim will know they are being controlled. You will know the role of the player you control."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/2/25/Witchmobile.png/revision/latest/scale-to-width-down/50?cb=20190110073324"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/e/e6/Witch.png/revision/latest?cb=20140716035354"
        override fun getColor(): Color {
            return Color(0xC15FFF)
        }
    },
    AMNESIAC("amnesiac"){
        override val classification = Classification.NB
        override val isUnique: Boolean = false
        override val alignment = Alignments.NEUTRAL_BENIGN
        override val attack = 0
        override val defence = 0
        override val summary = "A trauma patient that does not remember who he was."
        override val description = "Remember who you were by selecting a dead person"
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/a/ac/Amnemobile.png/revision/latest/scale-to-width-down/50?cb=20190110064107"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/2/2f/Forgetful_Freddy.png/revision/latest/scale-to-width-down/110?cb=20160826030733"
        override fun getColor(): Color {
            return Color(0x1BE8DD)
        }
    },
    SURVIVOR("survivor"){
        override val classification = Classification.NB
        override val isUnique: Boolean = false
        override val alignment = Alignments.NEUTRAL_BENIGN
        override val attack = 0
        override val defence = 0
        override val summary = "A Neutral character who just wants to live."
        override val description = "You may use a vest protecting yourself. Each of 4 vests can only be used once."
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/a/a9/Survmobile.png/revision/latest?cb=20190110072507"
        override val thumbnail = "https://orig00.deviantart.net/8843/f/2016/008/a/1/survivor_avatar_for_tos_by_nasubinori-d9n6u8b.png"
        override fun getColor(): Color {
            return Color(0xCACC55)
        }
    },
    VAMPIRE("vampire"){
        override val alignment = Alignments.NEUTRAL_CHAOS
        override val attack = 1
        override val defence = 0
        override val summary: String = "You are among the undead who wants to turn others at night."
        override val description : String = "Every other night you may attempt to convert another person to become a Vampire. If you already have 4 Vampires then this person gets killed. If your target is a Member of the Mafia or a Member of the Coven they will be killed regardless. "
        override val thumbnail: String = "https://vignette.wikia.nocookie.net/town-of-salem/images/4/4e/Vampire.png/revision/latest/scale-to-width-down/150?cb=20151101133009"
        override val icon: String = "https://vignette.wikia.nocookie.net/town-of-salem/images/5/59/Vampmobile.png/revision/latest?cb=20190110072818"
        override val isUnique: Boolean
            get() = false
        override val classification: String = Classification.NC
        override fun getColor(): Color {
            return Color(0x7B8969)
        }
    },
    /* Remove un-needed roles
    SELF("self"),
    GUILT("guilt"),
    */
    ADMIN("admin"){
        override val classification = "Administrator"
        override val alignment = Alignments.NULL
        override val summary: String
            get() = "null"
        override val description: String
            get() = "null"
        override val attack: Int //Max attack modifier accepted is 4 so value is 4 below max int value.
            get() = 2147483644
        override val defence: Int //Max defence modifier accepted is 4 so value is 4 below max int value.
            get() = 2147483644
        override val thumbnail = ""
        override val icon = ""
        override val isUnique: Boolean = false
        override fun getColor(): Color {
            return alignment.color
        }
    },
    JAILOR("jailor"){
        override val alignment = Alignments.TOWN
        override val attack = 3
        override val defence = 0
        override val summary = "A prison guard who secretly detains suspects."
        override val description = "You may choose one person during the day to jail for the night. You may anonymously talk with your prisoner. You can choose to attack your prisoner. The jailed target can't perform their night ability."
        override val isUnique: Boolean = true
        override val icon = "https://vignette.wikia.nocookie.net/town-of-salem/images/5/54/Jailormobile.png/revision/latest/scale-to-width-down/50?cb=20190110070535"
        override val thumbnail = "https://vignette.wikia.nocookie.net/town-of-salem/images/7/7e/Jailor.png/revision/latest/scale-to-width-down/150?cb=20151021224315"
        override val classification: String = Classification.TK
        override fun getColor(): Color {
            return alignment.color
        }
    }
}