package maxdistructo.discord.bots.botfather.commands.mafia.data

import org.json.JSONObject

object JSONStruct {
    data class UserData(
            val alive: Boolean,
            val attack: Int,
            val defence: Int,
            val framed: Boolean,
            val jailed: Boolean,
            val role: Roles,
            val permLevel: Int,
            val wins: Int,
            val losses: Int,
            val revealed: Boolean,
            val disguised: String,
            val cleaned: Boolean,
            val relay: String,
            val id: Long
    )
    fun userDataToJSON(userData: UserData): JSONObject {
        val obj = JSONObject()
        obj.put("alive", userData.alive)
        obj.put("attack", userData.attack)
        obj.put("defence", userData.defence)
        obj.put("framed", userData.framed)
        obj.put("jailed", userData.jailed)
        obj.put("role", userData.role.regName)
        obj.put("tier", userData.permLevel)
        obj.put("wins", userData.wins)
        obj.put("losses", userData.losses)
        obj.put("revealed", userData.revealed)
        obj.put("id", userData.id)
        obj.put("disguised", userData.disguised)
        obj.put("cleaned", userData.cleaned)
        obj.put("relay", userData.relay)
        return obj
    }
}