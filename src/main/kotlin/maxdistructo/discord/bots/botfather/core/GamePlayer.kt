package maxdistructo.discord.bots.botfather.core

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.User

abstract class GamePlayer(val user: User, val guild: Guild)