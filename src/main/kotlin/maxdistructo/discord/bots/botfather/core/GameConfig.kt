package maxdistructo.discord.bots.botfather.core

import maxdistructo.discord.core.Constants.s
import maxdistructo.discord.core.JSONUtils
import net.dv8tion.jda.api.entities.Guild
import org.json.JSONArray
import org.json.JSONObject

class GameConfig(val name: String, val guild: Guild) : ConfigHandler {
    init{
        readConfig()
    }
    override var config: JSONObject = JSONObject()
    private val filePath = s + "config/$name/${guild.id}.json"
    override fun readConfig(){
        config = JSONUtils.readJSONFromFile(filePath)
    }
    override fun writeConfig() {
        JSONUtils.writeJSONToFile(filePath, config)
    }
    override fun createConfig(){
        JSONUtils.createNewJSONFile(filePath)
    }
    override fun getKey(key: String): String {
        return config.getString(key)
    }
    override fun getJSONKey(key: String): JSONObject{
        return config.getJSONObject(key)
    }
    override fun getArrayKey(key: String): JSONArray{
        return config.getJSONArray(key)
    }
    override fun writeKey(key: String, value: String){
        config.put(key, value)
    }
    override fun writeKey(key: String, value: JSONObject){
        config.put(key, value)
    }
    override fun writeKey(key: String, value: JSONArray){
        config.put(key, value)
    }
}