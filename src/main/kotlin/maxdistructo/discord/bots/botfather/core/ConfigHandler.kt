package maxdistructo.discord.bots.botfather.core

import org.json.JSONArray
import org.json.JSONObject

interface ConfigHandler {
    var config: JSONObject
    fun readConfig()
    fun createConfig()
    fun writeConfig()
    fun getKey(key: String): String
    fun getJSONKey(key: String): JSONObject
    fun getArrayKey(key: String): JSONArray
    fun writeKey(key: String, value: String)
    fun writeKey(key: String, value: JSONObject)
    fun writeKey(key: String, value: JSONArray)
}