package maxdistructo.discord.bots.botfather.core

import net.dv8tion.jda.api.entities.Guild
import net.dv8tion.jda.api.entities.User

abstract class Game(val guild: Guild) {
    abstract val config: GameConfig
    abstract val playersList : MutableList<GamePlayer>
    abstract fun getPlayers(): List<User>
}